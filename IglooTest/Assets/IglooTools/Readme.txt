* Igloo360Camera

* IglooFPSController

The player is controlled using IglooFirstPersonController. A number of different inputs are available to control the movement and rotation of the player. The inputs are defined in IglooFirstPersonController as

movementInput 	- {STANDARD, GYROSC}
rotationInput 	- {STANDARD, GyrOSC or XIMU}
movementMode 	- {Walking, Flying, Flying_Ghost}

The IglooInputController is used to switch between these different inputs, switching is done using either OSC messages, key press or a UI menu. See IglooInputController.cs to see the methods of switching input.  

IglooInputXIMU and IglooInputGyroOSC should be added to the scene (These are attached to the IglooPlayer prefab) they receive OSC message input from the custom controllers used to control rotation and movement. 

The movement is controlled either by the standard Unity inputs (keyboard/xbox) or buttons with the gyrOSC app for iOS. See IglooFirstPersonController.cs

Movement mode affects how the player moves around the scene.

WALKING 		- Gravity on,	Colliders on
FLYING 			- Gravity off	Colliders on
FLYING_GHOST 	- Gravity off	Colliders off

* OSC Protocol (Receiving) 
'/gameEngine/rotationInput/mouse' 	- float (0-1) set rotation mouse
'/gameEngine/rotationInput/gyrOSC'	- float (0-1) set rotation gyrOSC
'/gameEngine/rotationInput/XIMU'	- float (0-1) set rotation XIMU

'/gameEngine/movementInput/unity'	- float (0-1) set movement Unity
'/gameEngine/movementInput/gyrOSC'	- float (0-1) set movement GyrOSC

'/gameEngine/movementMode/walking' 	- float (0-1) set movement Mode walking
'/gameEngine/movementMode/flying'	- float (0-1) set movement Mode flying
'/gameEngine/movementMode/ghost'	- float (0-1) set movement Mode ghost

'/ximu/euler'	- Three float parameters (0-1) with gyro data [0] - pitch , [1] - roll , [2] - (-)heading

'/unity/quit' - float (0-1) quit unity

'/gameEngine/setCameraType'	- 0: Cylinder, 1:Cube, 2:Unwrap, 3:Cylinder6

* OSC Protocol (Sending)
'/spout/reset'
 

'/camera/standardFOV'					- float (0-1) set FOV standard
'/camera/wideFOV'						- float (0-1) set FOV wide
'/camera/superWideFOV'					- float (0-1) set FOV superwide
'/gameEngine/3D/separation'				- float (0-1) set 3D separation 
'/gameEngine/3D/lensShift'				- float (0-1) set 3D lensShift
'/gameEngine/3D/enabled'				- float (0-1) set 3D enabled
'/gameEngine/3D/invert'					- float (0-1) set 3D invert
'/gameEngine/cameraTilting'				- float (0-1) set camera tilting On/Off
'/unity/sleep'							- float (0-1) set sleep On/Off
'/gameEngine/camera/farClipDistance'	- float (0-1) set Far clipping distance
*Keys

'o' - toggle 3D

* Command line arguments
To use command line arguments, create a shortcut of the built .exe, then right click > properties and add any arguments to the 'Target' text field.

The following command line arguments can be used:

'-igloo3DEnabled'			- Enables 3D Mode
'-iglooSpoutReset'			- Sender a reset message via OSC on creation
'-iglooSenderName' [NAME]	- Where the following [NAME] argument specifies the spout sharing name 
'-iglooCameraType' [NAME]	- Cylinder , Cube, Unwrap, Cylinder6







