﻿using extOSC;
using UnityEngine;
using System.Collections.Generic;

public class DataClientOSC : MonoBehaviour {

    private OSCReceiver oscIn;
    public int oscInPort = 9020;

    public bool getPosition = true;
    public bool getRotation = false;

    void Awake()
    {
        if (!oscIn) oscIn = gameObject.AddComponent<OSCReceiver>();
        oscIn.LocalPort = oscInPort;
        oscIn.Connect();
        // oscIn.filterDuplicates = true;
    }

    void Start()
    {
        oscIn.Bind("/player/transform", SetTransform);
    }

    void SetTransform(OSCMessage msg)
    {
        if (msg.ToArray(out List<OSCValue> arrayValues))
        {
            float pX = arrayValues[0].FloatValue;
            float pY = arrayValues[1].FloatValue;
            float pZ = arrayValues[2].FloatValue;
            float rX = arrayValues[3].FloatValue;
            float rY = arrayValues[4].FloatValue;
            float rZ = arrayValues[5].FloatValue;

            if (getPosition)
            {
                Vector3 position = new Vector3(pX, pY, pZ);
                gameObject.transform.position = position;
            }
            if (getRotation)
            {
                Vector3 rotation = new Vector3(rX, rY, rZ);
                gameObject.transform.rotation = Quaternion.Euler(rotation);
            }

        }

        //if (msg.Values[0](0, out pX) && msg.TryGet(1, out pY) && msg.TryGet(2, out pZ) && msg.TryGet(3, out rX) && msg.TryGet(4, out rY) && msg.TryGet(5, out rZ))
        //{
        //    Debug.Log("OSC Message Received: Postion: " + pX + " , " + pY + " , " + pZ + " Rotation:  " + rX + " , " + rY + " , " + rZ);
        //    if (getPosition)
        //    {
        //        Vector3 position = new Vector3(pX, pY, pZ);
        //        gameObject.transform.position = position;
        //    }
        //    if (getRotation)
        //    {
        //        Vector3 rotation = new Vector3(rX, rY, rZ);
        //        gameObject.transform.rotation = Quaternion.Euler(rotation);
        //    }
        //}
    }
}
