﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour {

    public float amount = 1.0f;
    public float offsetY = 0.0f;
    void Update()
    {
        Vector3 position = transform.position;
        position.y = Mathf.Sin(Time.time * 3) * amount + offsetY;
        transform.position = position;
    }
}
