﻿using extOSC;
using UnityEngine;

public class DataServerOSC : MonoBehaviour {

    private OSCTransmitter oscSender;
    public int sendPort = 9020;

    /// <summary>
    ///  Set this to the IP Address of the machine which is running the
    ///  instance of Unity with the Igloo 360 camera. 
    ///  This should be loaded from a settings file!
    ///  If you are running both instances of Unity on the same machine you 
    ///  can use the local host address "127.0.0.1"
    /// </summary>
    public string ipAddress = "192.168.0.1";

    void Awake()
    {
        if (!oscSender) oscSender = gameObject.AddComponent<OSCTransmitter>();
        oscSender.LocalHost = ipAddress;
        oscSender.LocalPort = sendPort;
        oscSender.Connect();
    }

    void LateUpdate()
    {
        Vector3 pos = gameObject.transform.position;
        Debug.Log(pos);
        Vector3 rot = gameObject.transform.rotation.eulerAngles;
        var message = new OSCMessage("/player/transform");
        message.AddValue(OSCValue.Float(pos.x));
        message.AddValue(OSCValue.Float(pos.y));
        message.AddValue(OSCValue.Float(pos.z));
        message.AddValue(OSCValue.Float(rot.x));
        message.AddValue(OSCValue.Float(rot.y));
        message.AddValue(OSCValue.Float(rot.z));
        oscSender.Send(message);
    }
}
