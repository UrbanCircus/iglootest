﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CubeData
{
    public float aX;
    public float aY;
    public float aZ;

    public float bX;
    public float bY;
    public float bZ;

    public float cX;
    public float cY;
    public float cZ;

    public float dX;
    public float dY;
    public float dZ;

    public float eX;
    public float eY;
    public float eZ;

    public float fX;
    public float fY;
    public float fZ;

    public float gX;
    public float gY;
    public float gZ;

    public float hX;
    public float hY;
    public float hZ;
}