﻿using UnityEngine;
using extOSC;

[ExecuteInEditMode]
public class CubeCalibrate : MonoBehaviour {

    // Calibration points needed to make up the viewports
    public Vector3[] calibrationPoints = new Vector3[4];

    // Viewports which will be passed into camera perspective calculation 
    [HideInInspector]
    public ViewPort[] viewPorts = new ViewPort[4];

    // Data container for saving/loading
    [HideInInspector]
    public CubeData cubeData;

    // OSC receiver
    private OSCReceiver oscIn;

    // Position vector used for setting calibration points
    public Transform markerTransform;
    
    void OnEnable()
    {
        // Register to load/save events
        IglooSaveController.OnLoad += Load;
        IglooSaveController.OnSave += Save;
    }

    void OnDisable()
    {
        // Unregister from load/save events
        IglooSaveController.OnLoad -= Load;
        IglooSaveController.OnSave -= Save;
    }

    void Awake()
    {
        //Load();
        oscIn = this.gameObject.AddComponent<OSCReceiver>();//OscSimplUtils.Open(9007, oscIn, this.gameObject);
        oscIn.LocalPort = 9007;
        oscIn.Bind("/gameEngine/cube/setVertex", SetVertexOSC);

        if (markerTransform == null)
        {
            // Debug.LogError("Marker transform not assigned - cube calibration will not be possible");
        }
    }

    void Update()
    {
        SetViewports();
    }

    public void SetVertexOSC(OSCMessage message) 
    {
        Debug.Log(message);
        int index = message.Values[0].IntValue;

        if (index > 0 && index < calibrationPoints.Length+1)
        {
            SetVert(index-1, markerTransform.position);
        }
    }

    // Saves position of vertex (i), which makes up the cube
    public bool SetVert(int i, Vector3 pos)
    {
        if (i < 0 || i > calibrationPoints.Length)
        {
            Debug.Log("Vertex out of range");
            return false;
        }
        calibrationPoints[i] = pos;
        //Save();
        SetViewports();
        return true;
    }

    // Sets the viewports dimensions based on calibration points
    void SetViewports()
    {
        Vector3 currentPos = gameObject.transform.position;

        for (int i=0; i<viewPorts.Length; i++)
        {
            switch (i) {
                case 0: 
                    viewPorts[i].bottomLeft     = calibrationPoints[0] + currentPos; // BL
                    viewPorts[i].bottomRight    = new Vector3(calibrationPoints[1].x + currentPos.x, calibrationPoints[0].y + currentPos.y, calibrationPoints[1].z + currentPos.z); 
                    viewPorts[i].topLeft        = new Vector3(calibrationPoints[0].x + currentPos.x, calibrationPoints[1].y + currentPos.y, calibrationPoints[0].z + currentPos.z); 
                    viewPorts[i].topRight       = calibrationPoints[1] + currentPos; // TR
                    viewPorts[i].center         = (calibrationPoints[1]- calibrationPoints[0]) * 0.5f; // Midpoint between BL and TR
                    break;
                case 1:
                    viewPorts[i].bottomLeft     = new Vector3(calibrationPoints[1].x + currentPos.x, calibrationPoints[2].y + currentPos.y, calibrationPoints[1].z + currentPos.z); 
                    viewPorts[i].bottomRight    = calibrationPoints[2] + currentPos; // BR
                    viewPorts[i].topLeft        = calibrationPoints[1] + currentPos; // TL
                    viewPorts[i].topRight       = new Vector3(calibrationPoints[2].x + currentPos.x, calibrationPoints[1].y + currentPos.y, calibrationPoints[2].z + currentPos.z); 
                    viewPorts[i].center         = (calibrationPoints[2] - calibrationPoints[1]) * 0.5f; // Midpoint between TL and BR
                    break;
                case 2:
                    viewPorts[i].bottomLeft     = calibrationPoints[2] + currentPos; // BL
                    viewPorts[i].bottomRight    = new Vector3(calibrationPoints[3].x + currentPos.x, calibrationPoints[2].y + currentPos.y, calibrationPoints[3].z + currentPos.z); 
                    viewPorts[i].topLeft        = new Vector3(calibrationPoints[2].x + currentPos.x, calibrationPoints[3].y + currentPos.y, calibrationPoints[2].z + currentPos.z); 
                    viewPorts[i].topRight       = calibrationPoints[3] + currentPos; // TR
                    viewPorts[i].center         = (calibrationPoints[3] - calibrationPoints[2]) * 0.5f; // Midpoint between BL and TR
                    break;
                case 3:
                    viewPorts[i].bottomLeft     = new Vector3(calibrationPoints[3].x + currentPos.x, calibrationPoints[0].y + currentPos.y, calibrationPoints[3].z + currentPos.z); 
                    viewPorts[i].bottomRight    = calibrationPoints[0] + currentPos; // BR
                    viewPorts[i].topLeft        = calibrationPoints[3] + currentPos; // TL
                    viewPorts[i].topRight       = new Vector3(calibrationPoints[0].x + currentPos.x, calibrationPoints[3].y + currentPos.y, calibrationPoints[0].z + currentPos.z); // TR
                    viewPorts[i].center         = (calibrationPoints[0] - calibrationPoints[3]) * 0.5f; // Midpoint between TL and BR
                    break;
            }
        }
    }

    // Saves calibrations points to csv
    public void Save()
    {
        cubeData.aX = calibrationPoints[0].x;
        cubeData.aY = calibrationPoints[0].y;
        cubeData.aZ = calibrationPoints[0].z;

        cubeData.bX = calibrationPoints[1].x;
        cubeData.bY = calibrationPoints[1].y;
        cubeData.bZ = calibrationPoints[1].z;

        cubeData.cX = calibrationPoints[2].x;
        cubeData.cY = calibrationPoints[2].y;
        cubeData.cZ = calibrationPoints[2].z;

        cubeData.dX = calibrationPoints[3].x;
        cubeData.dY = calibrationPoints[3].y;
        cubeData.dZ = calibrationPoints[3].z;

        if (cubeData != null)
        {
            CubeSaveLoad.cubeSave = cubeData;
            CubeSaveLoad.Save();
            Debug.Log("Saved");
        }
        else Debug.LogError("Cube Data is NULL");
    }

    // Loads calibration points from csv
    public void Load()
    {
        if (CubeSaveLoad.Load())
        {
            cubeData = CubeSaveLoad.cubeSave;

            Debug.Log(cubeData);

            calibrationPoints[0].x = cubeData.aX;
            calibrationPoints[0].y = cubeData.aY;
            calibrationPoints[0].z = cubeData.aZ;

            calibrationPoints[1].x = cubeData.bX;
            calibrationPoints[1].y = cubeData.bY;
            calibrationPoints[1].z = cubeData.bZ;

            calibrationPoints[2].x = cubeData.cX;
            calibrationPoints[2].y = cubeData.cY;
            calibrationPoints[2].z = cubeData.cZ;

            calibrationPoints[3].x = cubeData.dX;
            calibrationPoints[3].y = cubeData.dY;
            calibrationPoints[3].z = cubeData.dZ;
        }
        else
        {
            Debug.LogError("No file found, saving defaults");
            Save();
        }
        SetViewports();
    }

    public struct ViewPort
    {
        public Vector3 bottomLeft;
        public Vector3 bottomRight;
        public Vector3 topLeft;
        public Vector3 topRight;
        public Vector3 center;
    }
}
