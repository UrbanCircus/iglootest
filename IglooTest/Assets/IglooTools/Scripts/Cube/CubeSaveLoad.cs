﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class CubeSaveLoad
{

    public static CubeData cubeSave;

    public static void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Path.Combine(Application.persistentDataPath, "iglooCube.ig"));
        bf.Serialize(file, CubeSaveLoad.cubeSave);
        file.Close();
    }

    public static bool Load()
    {
        if (File.Exists(Path.Combine(Application.persistentDataPath, "iglooCube.ig")))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath, "iglooCube.ig"), FileMode.Open);
            CubeSaveLoad.cubeSave = (CubeData)bf.Deserialize(file);
            file.Close();
            return true;
        }
        else return false;
    }
}
