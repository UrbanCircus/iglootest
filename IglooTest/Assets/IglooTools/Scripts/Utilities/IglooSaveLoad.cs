﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class IglooSaveLoad {

    public static IglooData iglooSave;

    public static void SaveIglooData() {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Path.Combine(Application.persistentDataPath, "iglooSave.ig"));
        bf.Serialize(file, IglooSaveLoad.iglooSave);
        file.Close();
    }

    public static bool LoadIglooData() {
        if (File.Exists(Path.Combine(Application.persistentDataPath, "iglooSave.ig"))) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath, "iglooSave.ig"), FileMode.Open);
            IglooSaveLoad.iglooSave = (IglooData)bf.Deserialize(file);
            file.Close();
            return true;
        }
        else return false;
    }
}
