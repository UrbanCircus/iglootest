﻿using UnityEngine;
using System.Collections;

public class MoveBalls : MonoBehaviour {

    public float amount = 1.0f;
    public float offsetY = 0.0f;
    public float spinSpeed = 1.0f;
	void Update () {
		Vector3 position = transform.position;
		position.y = Mathf.Sin (Time.time*3) * amount + offsetY;
		transform.position = position;
		transform.Rotate (0, spinSpeed * Time.deltaTime, 0);
	}
}
