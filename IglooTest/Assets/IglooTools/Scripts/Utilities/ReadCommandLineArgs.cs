﻿using UnityEngine;
using System.Collections;
using System;

public static class ReadCommandLineArgs {

	public static bool ArgumentExists(string _argument) {
        bool argFound = false;
        string[] cmdLineArgs = System.Environment.GetCommandLineArgs();
        foreach (string _arg in cmdLineArgs) {
            if (_arg == _argument) argFound = true;
        }
        return argFound;
	}

    public static string GetValueFollowingArgument(string _argument)
    {
        string[] args = System.Environment.GetCommandLineArgs();
        string name = string.Empty;
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i] == _argument)
            {
                name = args[i + 1];
            }
        }
        return name;
    }

}
