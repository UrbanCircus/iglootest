﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class IglooData
{
    /*Igloo Camera data*/
    public float    cameraSeparation3D;
    public float    cameraLensShift3D;
    //public IglooCamera.IglooFieldOfView cameraFOV;

    /*Igloo FPS data*/
    public IglooFirstPersonController.MOVEMENT_INPUT    playerMovementInput;
    public IglooFirstPersonController.MOVEMENT_MODE     playerMovementMode;
    public IglooFirstPersonController.ROTATION_INPUT    playerRotationInput;
    public bool invertPitch;

}
