﻿using UnityEngine;

namespace Igloo
{
    public class SpoutSenderShaderPass : MonoBehaviour
    {
        private string sharingName = "UnitySender";

        private RenderTexture sendTexture;
        public RenderTexture camTexture;
        public Material unwrapMat;

        private Klak.Spout.SpoutSender spoutSender;
        private Klak.Ndi.NdiSender ndiSender;

        private bool senderIsCreated = false;

        void Update()
        {
            if (!senderIsCreated) return;

            if (sendTexture == null)
            {
                InitailiseTexture(camTexture.width, camTexture.height);
                return;
            }

            if (senderIsCreated)
            {
                if (camTexture.width != sendTexture.width || camTexture.height != sendTexture.height) {
                    InitailiseTexture(camTexture.width, camTexture.height);
                }
                Graphics.Blit(camTexture, sendTexture, unwrapMat);
            }
        }

        public void CreateSender(string name)
        {
            sharingName = name;

            if (sendTexture == null)
            {
                Debug.Log("Texture is null");
                InitailiseTexture(camTexture.width, camTexture.height);
            }

            if (sendTexture == null) return;

            if (!senderIsCreated && GetComponent<IglooCamera>().iglooStreamSender == IglooCamera.StreamSender.Spout)
            {
                spoutSender = gameObject.AddComponent<Klak.Spout.SpoutSender>();
                spoutSender.senderName = name;
                spoutSender.sourceTexture = sendTexture;
                senderIsCreated = true;
            }
            else if (!senderIsCreated && GetComponent<IglooCamera>().iglooStreamSender == IglooCamera.StreamSender.NDI)
            {
                ndiSender = gameObject.AddComponent<Klak.Ndi.NdiSender>();
                ndiSender.sourceTexture = sendTexture;
                senderIsCreated = true;
            }
        }

        public void InitailiseTexture(int w, int h)
        {
            if (sendTexture != null) sendTexture.Release();
            sendTexture = null;
            RenderTexture tex = new RenderTexture(w, h, 24);
            tex.Create();
            tex.name = sharingName + "Texture";
            sendTexture = tex;
        }

        public void AdjustImageY(float adjustAmount)
        {
            unwrapMat.SetFloat("_Scale", adjustAmount);
        }
    }
}
