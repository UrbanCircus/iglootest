﻿using System;
using UnityEngine;


[Serializable]
public class IglooLookGYROSC : IglooLook  {
	
	public override void LookRotation(Transform character, Transform camera, bool invertPitch)
	{
		float yRot = IglooInputGyrOSC.Instance.Heading;
		float xRot = IglooInputGyrOSC.Instance.Pitch;

        if (invertPitch) xRot = -xRot;

        m_CharacterTargetRot = Quaternion.Euler(0f, yRot, 0f);
		m_CameraTargetRot = Quaternion.Euler (xRot, 0f, 0f);

		if(smooth)
		{
			character.localRotation = Quaternion.Slerp (character.localRotation, m_CharacterTargetRot,
				                                        smoothTime * Time.deltaTime);
			camera.localRotation = Quaternion.Slerp (camera.localRotation, m_CameraTargetRot,
				                                        smoothTime * Time.deltaTime);
		}
		else
		{
			character.localRotation = m_CharacterTargetRot;
			camera.localRotation = m_CameraTargetRot;
		}
	}

}

