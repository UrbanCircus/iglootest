﻿using System;
using UnityEngine;
//using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;

[RequireComponent(typeof (CharacterController))]
[RequireComponent(typeof (AudioSource))]
public class IglooFirstPersonController : MonoBehaviour
{
	public enum ROTATION_INPUT {STANDARD,GYROSC,XIMU,};
	[Header("Igloo Settings")]
	public ROTATION_INPUT rotationInput = ROTATION_INPUT.STANDARD;

	public enum MOVEMENT_INPUT {STANDARD, GYROSC};
	public MOVEMENT_INPUT movementInput = MOVEMENT_INPUT.STANDARD;

	public enum MOVEMENT_MODE {WALKING,FLYING,FLYING_GHOST};
	public MOVEMENT_MODE movementMode = MOVEMENT_MODE.WALKING;

	// Cached Values for changing movement modes
	private MOVEMENT_MODE cachedMovmentMode = MOVEMENT_MODE.WALKING;


	private float cached_m_GravityMultiplier = 0.0f;
	private float cached_m_StickToGroundForce = 0.0f;

	[Header("Character Controller Settings")]
	[SerializeField] private bool m_IsWalking = false;
	[SerializeField] private float m_WalkSpeed = 5f; // maximum speed of walking
	[SerializeField] private float m_RunSpeed = 10f;
    [SerializeField] private float m_Climbspeed = 0.0f;
	[SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten = 0.0f;
	[SerializeField] private float m_JumpSpeed = 10f;
	[SerializeField] private float m_StickToGroundForce = 2f;
	[SerializeField] private float m_GravityMultiplier = 1f;
	[SerializeField] private IglooLookGYROSC m_IglooLookGyrOSC = null;
	[SerializeField] private IglooLookXIMU m_IglooLookXIMU = null;
	[SerializeField] private IglooMouseLook m_MouseLook = null;
	[SerializeField] private bool m_UseFovKick = false;
	[SerializeField] private IglooFOVKick m_FovKick = new IglooFOVKick();
	[SerializeField] private bool m_UseHeadBob = false;
	[SerializeField] private IglooCurveControlledBob m_HeadBob = new IglooCurveControlledBob();
	[SerializeField] private IglooLerpControlledBob m_JumpBob = new IglooLerpControlledBob();
	[SerializeField] private float m_StepInterval = 5f;
	[SerializeField] private AudioClip[] m_FootstepSounds = null;    // an array of footstep sounds that will be randomly selected from.
	[SerializeField] private AudioClip m_JumpSound = null;           // the sound played when character leaves the ground.
	[SerializeField] private AudioClip m_LandSound = null;           // the sound played when character touches back on ground.
    public enum CROUCH_MODE { ONE, TWO, THREE};
    public CROUCH_MODE crouchMode = CROUCH_MODE.ONE;
    [SerializeField] private float m_crouchMin = -0.4f;
    [SerializeField] private float m_crouchMax = 0.2f;

    private Camera m_Camera;
	private bool m_Jump;
	private float m_YRotation;
	private Vector2 m_Input;
	private Vector3 m_MoveDir = Vector3.zero;
	private CharacterController m_CharacterController;
	private CollisionFlags m_CollisionFlags;
	private bool m_PreviouslyGrounded;
	private Vector3 m_OriginalCameraPosition;
	private float m_StepCycle;
	private float m_NextStep;
	private bool m_Jumping;
	private AudioSource m_AudioSource;
    private float lift = 0.0f;
    public bool m_invertPitch = false;
    private float curWalkSpeed;

    // Use this for initialization
    private void Start()
	{
		if (IglooInputController.Instance!=null) {
			IglooInputController.Instance.registerPlayer(this);
		}
		else Debug.Log ("IglooFirstPersonController - add IglooInputController to scene for switching inputs");

		m_CharacterController = GetComponent<CharacterController>();
		m_Camera = GetComponentInChildren<Camera>();
		if (m_Camera == null) {
			Debug.LogError("Can't find the camera");
			Debug.Break();
		}
		m_OriginalCameraPosition = m_Camera.transform.localPosition;
		m_FovKick.Setup(m_Camera);
		m_HeadBob.Setup(m_Camera, m_StepInterval);
		m_StepCycle = 0f;
		m_NextStep = m_StepCycle/2f;
		m_Jumping = false;
		m_AudioSource = GetComponent<AudioSource>();
		m_MouseLook.Init(transform , m_Camera.transform);
		m_IglooLookGyrOSC.Init(transform , m_Camera.transform);
		m_IglooLookXIMU.Init(transform , m_Camera.transform);
	}


    #region Ladder System, Collider interaction.

    private bool _isClimbing = false;
    private Transform _ladderTrigger;

    private void OnTriggerEnter(Collider col)
    {
        if(col.tag == "ladder bottom" || col.tag == "ladder top") _ladderTrigger = col.transform;

        if (col.tag == "ladder bottom")
        {
            if (!_isClimbing)
            {
                // player is not climbing the ladder, and wants to.
                // Check for player interaction
                // Prompt for Keypress
                // Check for Keypress (controller A)
                _climbingTransition = TransitionState.ToLadder1;
                ToggleClimbing();
            }
            else
            {
                // player has reached the bottom of the ladder, whilst climbing.
                // Player detatch
                ToggleClimbing();
                _climbingTransition = TransitionState.None;
            }
        }


        if (col.tag == "ladder top")
        {
            // if player hit the top of the ladder, whilst climbing it.
            if (_isClimbing)
            {
                // move to the upper point and exit the ladder.
                _climbingTransition = TransitionState.ToLadder3;
            }
            else
            {
                // player has come from above, and wants to go down the ladder
                // Check for player interaction
                // Make ladder selectable and glow. 
                // Prompt for Keypress
                // Check for Keypress (controller A)
                _climbingTransition = TransitionState.ToLadder2;
            }

            ToggleClimbing();

        }

        if (col.tag == "Respawn")
        {
            throw new NotImplementedException("Death System");
        }
    }

    private void ToggleClimbing()
    {
        _isClimbing = !_isClimbing;
        //ladderIconToggle = !ladderIconToggle;
        //_ladderIconInUse.SetActive(ladderIconToggle);
    }

    private enum TransitionState
    {
        None = 0,
        ToLadder1 = 1,
        ToLadder2 = 2,
        ToLadder3 = 3
    }

    private TransitionState _climbingTransition = TransitionState.None;
    // private bool ladderIconToggle = false;
    private GameObject _ladderIconInUse;

    #endregion


    // Update is called once per frame
    private void Update()
	{
		if (movementMode != cachedMovmentMode) UpdateMovementMode ();

		RotateView();
		// the jump state needs to read here to make sure it is not missed
		if (!m_Jump)
		{
			m_Jump = Input.GetButtonDown("Jump");
		}
        try
        {
            if (Input.GetButtonDown("XboxB"))
            {
                _isClimbing = false;
            }
        }
        catch {

        }

			
		if (!m_PreviouslyGrounded && m_CharacterController.isGrounded)
		{
			StartCoroutine(m_JumpBob.DoBobCycle());
			PlayLandingSound();
			m_MoveDir.y = 0f;
			m_Jumping = false;
		}
		if (!m_CharacterController.isGrounded && !m_Jumping && m_PreviouslyGrounded)
		{
			m_MoveDir.y = 0f;
		}
				
		m_PreviouslyGrounded = m_CharacterController.isGrounded;

		if (movementMode != cachedMovmentMode) UpdateMovementMode ();

	}

	private void UpdateMovementMode () {
		switch (movementMode) 
		{
		case MOVEMENT_MODE.FLYING:
			GetComponent<Rigidbody>().useGravity = false;
			if (cachedMovmentMode==MOVEMENT_MODE.WALKING){
				cached_m_GravityMultiplier = m_GravityMultiplier;
				cached_m_StickToGroundForce = m_StickToGroundForce;
			}
			m_StickToGroundForce = 0;
			m_GravityMultiplier = 0;
			break;
				
		case MOVEMENT_MODE.FLYING_GHOST:
			GetComponent<Rigidbody>().useGravity = false;
			if (cachedMovmentMode==MOVEMENT_MODE.WALKING){
				cached_m_GravityMultiplier = m_GravityMultiplier;
				cached_m_StickToGroundForce = m_StickToGroundForce;
			}
			m_StickToGroundForce = 0;
			m_GravityMultiplier = 0;
			break;
				
		case MOVEMENT_MODE.WALKING:
			GetComponent<Rigidbody>().useGravity = true;
			m_GravityMultiplier = cached_m_GravityMultiplier;
			m_StickToGroundForce = cached_m_StickToGroundForce;
			break;
		}
		cachedMovmentMode = movementMode;
	}
		
		
	private void PlayLandingSound()
	{
		m_AudioSource.clip = m_LandSound;
		m_AudioSource.Play();
		m_NextStep = m_StepCycle + .5f;
	}
		
		
	private void FixedUpdate()
	{
		if (movementMode != cachedMovmentMode) UpdateMovementMode ();

		float speed = 0f;
        GetInput(out float curspeed);

        if(curspeed > speed)
        {
            speed = Mathf.Lerp(speed, curspeed, m_WalkSpeed * Time.smoothDeltaTime);
        }
        else if(speed > curspeed)
        {
            speed = Mathf.Lerp(curspeed, speed, m_WalkSpeed * Time.smoothDeltaTime);
        }
		// always move along the camera forward as it is the direction that it being aimed at
		Vector3 desiredMove = m_Camera.transform.forward*m_Input.y + m_Camera.transform.right*m_Input.x;

		// get a normal for the surface that is being touched to move along it
		Physics.SphereCast(transform.position, m_CharacterController.radius, Vector3.down, out RaycastHit hitInfo,
			                m_CharacterController.height/2f);
		// Igloo - Remove as seems to stop analogue movement
		//desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;
			
		m_MoveDir.x = desiredMove.x*speed;
		if (movementMode == MOVEMENT_MODE.FLYING || movementMode == MOVEMENT_MODE.FLYING_GHOST) {
            if (lift == 0) m_MoveDir.y = desiredMove.y * speed; // Igloo - Added for Flying Ability
            else m_MoveDir.y = lift * speed;
        }
		m_MoveDir.z = desiredMove.z*speed;



        #region Ladder Climbing Logic

        Vector3 ladderDesiredMove = Vector3.zero;

        if (_isClimbing)
        {

            Transform trLadder = _ladderTrigger.parent;

            if (_climbingTransition != TransitionState.None)
            {
                print("First IF - Climbing Transition" + _climbingTransition.ToString());

                // Move the player to the next point along their current path
                transform.position = trLadder.Find(_climbingTransition.ToString()).position;

                _climbingTransition = TransitionState.None;
            }
            else
            {
                print("First Else - Climbing Transition " + " Rotation system.");
                // Attach player to the ladder, with the rotation angle of the ladder's transform
                ladderDesiredMove = trLadder.rotation * Vector3.forward * m_Input.y;

                m_MoveDir.y = ladderDesiredMove.y * m_Climbspeed;
                m_MoveDir.x = ladderDesiredMove.x * m_Climbspeed;
                m_MoveDir.z = ladderDesiredMove.z * m_Climbspeed;

                m_CollisionFlags = m_CharacterController.Move(m_MoveDir * Time.fixedDeltaTime);
            }
        }
        else
        {
            //print("Second Else - Movement system");

            ladderDesiredMove = transform.forward * m_Input.y + transform.right * m_Input.x;

            // find the normal for the surface that is being touched to move along it
            Physics.SphereCast(transform.position, m_CharacterController.radius, Vector3.down, out hitInfo, m_CharacterController.height / 2f);
            ladderDesiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

            m_MoveDir.x = ladderDesiredMove.x * speed;
            m_MoveDir.z = ladderDesiredMove.z * speed;

            if (m_CharacterController.isGrounded)
            {
                m_MoveDir.y = -m_StickToGroundForce;
            }
        }



        #endregion

        if (movementMode != MOVEMENT_MODE.FLYING_GHOST) {
			if (m_CharacterController.isGrounded)
			{
				m_MoveDir.y = -m_StickToGroundForce;
					
				if (m_Jump)
				{
					m_MoveDir.y = m_JumpSpeed;
					PlayJumpSound();
					m_Jump = false;
					m_Jumping = true;
				}
			}
			else
			{
                    m_MoveDir += Physics.gravity * m_GravityMultiplier * Time.smoothDeltaTime;
			}
		}

		if (!PlayerFrozen) {
			if (movementMode != MOVEMENT_MODE.FLYING_GHOST)
				m_CollisionFlags = m_CharacterController.Move(m_MoveDir*Time.fixedDeltaTime);
				
			else if (movementMode == MOVEMENT_MODE.FLYING_GHOST) {
				transform.Translate(m_MoveDir * Time.fixedDeltaTime,Space.World);
			}
		}


        ProgressStepCycle(speed);
        UpdateCameraPosition(speed);
        UpdateCrouch();

    }
		
		
	private void PlayJumpSound()
	{
		m_AudioSource.clip = m_JumpSound;
		m_AudioSource.Play();
	}
	
    void Teleport(Transform floorNode)
    {
        gameObject.transform.position = floorNode.position;
    }	
		
	private void ProgressStepCycle(float speed)
	{
		if (m_CharacterController.velocity.sqrMagnitude > 0 && (m_Input.x != 0 || m_Input.y != 0))
		{
			m_StepCycle += (m_CharacterController.velocity.magnitude + (speed*(m_IsWalking ? 1f : m_RunstepLenghten)))*
				Time.fixedDeltaTime;
		}
			
		if (!(m_StepCycle > m_NextStep))
		{
			return;
		}
			
		m_NextStep = m_StepCycle + m_StepInterval;
		if (m_FootstepSounds.Length != 0) PlayFootStepAudio();
	}
		
		
	private void PlayFootStepAudio()
	{
		if (!m_CharacterController.isGrounded)
		{
			return;
		}
		// pick & play a random footstep sound from the array,
		// excluding sound at index 0
		int n = Random.Range(1, m_FootstepSounds.Length);
		m_AudioSource.clip = m_FootstepSounds[n];
		m_AudioSource.PlayOneShot(m_AudioSource.clip);
		// move picked sound to index 0 so it's not picked next time
		m_FootstepSounds[n] = m_FootstepSounds[0];
		m_FootstepSounds[0] = m_AudioSource.clip;
	}
		
		
	private void UpdateCameraPosition(float speed)
	{
		Vector3 newCameraPosition;
		if (!m_UseHeadBob)
		{
			return;
		}
		if (m_CharacterController.velocity.magnitude > 0 && m_CharacterController.isGrounded)
		{
			m_Camera.transform.localPosition =
				m_HeadBob.DoHeadBob(m_CharacterController.velocity.magnitude +
					                (speed*(m_IsWalking ? 1f : m_RunstepLenghten)));
			newCameraPosition = m_Camera.transform.localPosition;
			newCameraPosition.y = m_Camera.transform.localPosition.y - m_JumpBob.Offset();
		}
		else
		{
			newCameraPosition = m_Camera.transform.localPosition;
			newCameraPosition.y = m_OriginalCameraPosition.y - m_JumpBob.Offset();
		}
		m_Camera.transform.localPosition = newCameraPosition;
	}


    private void UpdateCrouch() {
        if (movementMode == MOVEMENT_MODE.WALKING) {
            float min;
            float max;
            Vector3 newCameraPosition = m_Camera.transform.localPosition;

            switch (crouchMode) {
                case CROUCH_MODE.ONE:
                    newCameraPosition.y = Mathf.Lerp(newCameraPosition.y, Mathf.Clamp(newCameraPosition.y + lift, m_crouchMin, m_crouchMax), Time.deltaTime*3);
                    m_Camera.transform.localPosition = newCameraPosition;
                    break;
                case CROUCH_MODE.TWO:
                    min = m_OriginalCameraPosition.y + m_crouchMin;
                    max = m_OriginalCameraPosition.y + m_crouchMax;
                    if (lift == 0)
                    {
                        newCameraPosition.y = Mathf.Lerp(newCameraPosition.y, m_OriginalCameraPosition.y, Time.deltaTime * 3);
                        m_Camera.transform.localPosition = newCameraPosition;
                    }
                    else if (lift < 0)
                    {
                        newCameraPosition.y = Mathf.Lerp(newCameraPosition.y, min, Time.deltaTime * -lift * 3);
                        m_Camera.transform.localPosition = newCameraPosition;
                    }
                    else if (lift > 0)
                    {
                        newCameraPosition.y = Mathf.Lerp(newCameraPosition.y, max, Time.deltaTime * lift * 3);
                        m_Camera.transform.localPosition = newCameraPosition;
                    }
                    break;
                case CROUCH_MODE.THREE:
                    min = m_OriginalCameraPosition.y - (m_crouchMin * lift);
                    max = m_OriginalCameraPosition.y + (m_crouchMax * lift);
                    if (lift == 0)
                    {
                        newCameraPosition.y = Mathf.Lerp(newCameraPosition.y, m_OriginalCameraPosition.y, Time.deltaTime * 3);
                        m_Camera.transform.localPosition = newCameraPosition;
                    }
                    else if (lift < 0)
                    {
                        newCameraPosition.y = Mathf.Lerp(newCameraPosition.y, min, Time.deltaTime *3);
                        m_Camera.transform.localPosition = newCameraPosition;
                    }
                    else if (lift > 0)
                    {
                        newCameraPosition.y = Mathf.Lerp(newCameraPosition.y, max, Time.deltaTime * 3);
                        m_Camera.transform.localPosition = newCameraPosition;
                    }
                    break;
                default:
                    break;
            }
        }
    }	
		
	private void GetInput(out float speed)
	{
		float horizontal = 0.0f;
		float vertical = 0.0f;		// Read input
		if (movementInput == MOVEMENT_INPUT.STANDARD)
        {
			horizontal = Input.GetAxis("Horizontal");
			vertical = Input.GetAxis("Vertical");
            lift = Input.GetAxis("Igloo_Lift");
		}
		else if (movementInput == MOVEMENT_INPUT.GYROSC){
			horizontal = IglooInputGyrOSC.Instance.MovememtAxis.z;
			vertical = IglooInputGyrOSC.Instance.MovememtAxis.x;
            lift = IglooInputGyrOSC.Instance.MovememtAxis.y;
        }
			
		bool waswalking = m_IsWalking;

#if !MOBILE_INPUT
        // On standalone builds, walk/run speed is modified by a key press.
        // keep track of whether or not the character is walking or running
        //m_IsWalking = !Input.GetKey(KeyCode.LeftShift); // Igloo - removed
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.JoystickButton4))
        {
            m_IsWalking = false;
        }
        else
            m_IsWalking = true;
        #endif
        // set the desired speed to be walking or running
        speed = m_IsWalking ? m_WalkSpeed : m_RunSpeed;
		m_Input = new Vector2(horizontal, vertical);
			
		// normalize input if it exceeds 1 in combined length:
		if (m_Input.sqrMagnitude > 1)
		{
			m_Input.Normalize();
		}
			
		// handle speed change to give an fov kick
		// only if the player is going to a run, is running and the fovkick is to be used
		if (m_IsWalking != waswalking && m_UseFovKick && m_CharacterController.velocity.sqrMagnitude > 0)
		{
			StopAllCoroutines();
			StartCoroutine(!m_IsWalking ? m_FovKick.FOVKickUp() : m_FovKick.FOVKickDown());
		}
	}
		
		
	private void RotateView()
	{
		if (!PlayerFrozen) {
			switch (rotationInput)
			{
			case ROTATION_INPUT.STANDARD:
				m_MouseLook.LookRotation (transform, m_Camera.transform, m_invertPitch);
				break;
			case ROTATION_INPUT.GYROSC:
				m_IglooLookGyrOSC.LookRotation (transform, m_Camera.transform, m_invertPitch);
				break;
			case ROTATION_INPUT.XIMU:
				m_IglooLookXIMU.LookRotation (transform, m_Camera.transform, m_invertPitch);
				break;
			}
		}
	}
		
		
	private void OnControllerColliderHit(ControllerColliderHit hit)
	{
		Rigidbody body = hit.collider.attachedRigidbody;
		//dont move the rigidbody if the character is on top of it
		if (m_CollisionFlags == CollisionFlags.Below)
		{
			return;
		}
			
		if (body == null || body.isKinematic)
		{
			return;
		}
		body.AddForceAtPosition(m_CharacterController.velocity*0.1f, hit.point, ForceMode.Impulse);
	}

    public bool PlayerFrozen { get; set; } = false;

}
