﻿using System;
using UnityEngine;

[Serializable]
public class IglooLook
{

	public bool clampVerticalRotation = false;
	public float MinimumX = -90F;
	public float MaximumX = 90F;
	public bool smooth;
	public float smoothTime = 5f;

	protected Quaternion m_CharacterTargetRot;
	protected Quaternion m_CameraTargetRot;
		
		
	public virtual void Init(Transform character, Transform camera)
	{
		m_CharacterTargetRot = character.localRotation;
		m_CameraTargetRot = camera.localRotation;
	}
		
		
	public virtual void LookRotation(Transform character, Transform camera, bool invertPitch)
	{

	}
		
}

