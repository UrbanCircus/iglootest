﻿using System;
using UnityEngine;

[Serializable]
public class IglooLookXIMU : IglooLook  {
		
	public override void LookRotation(Transform character, Transform camera, bool invertPitch)
	{
		float yRot = IglooInputXIMU.Instance.Heading;
		float xRot = IglooInputXIMU.Instance.Pitch;

        if (invertPitch) xRot = -xRot;

         m_CharacterTargetRot = Quaternion.Euler (0, yRot, 0f);
		m_CameraTargetRot = Quaternion.Euler (xRot, 0f, 0f);
			
		if(smooth)
		{
			character.localRotation = Quaternion.Slerp (character.localRotation, m_CharacterTargetRot,
				                                        smoothTime * Time.deltaTime);
			camera.localRotation = Quaternion.Slerp (camera.localRotation, m_CameraTargetRot,
				                                        smoothTime * Time.deltaTime);
		}
		else
		{
			character.localRotation = m_CharacterTargetRot;
			camera.localRotation = m_CameraTargetRot;
		}
	}
		
}

