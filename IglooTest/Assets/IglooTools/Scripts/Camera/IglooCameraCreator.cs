﻿using UnityEngine;
using extOSC;
using Klak.Spout;
using System;

public class IglooCameraCreator : MonoBehaviour
{
    #region Public Varibles
    public enum CameraType {Cylinder, Cube, Unwrap, Cylinder6, ImmersiveRoom4, None}
    public CameraType defaultCamera = CameraType.Cylinder;
    private CameraType activeCamera = CameraType.None;
    public enum StreamType {Spout, NDI};
    public StreamType streamType = StreamType.Spout;

    public GameObject[] iglooCameras;
    [System.Serializable]
    public struct AdditionalSettings
    {
        public Transform followTransform;
        public GameObject graphy;
    }
    public AdditionalSettings additionalSettings;


    #endregion

    #region Private Varibles
    private OSCReceiver oscIn;
    private OSCTransmitter oscOut;
    private bool isSetup = false;
    private bool isSendSpoutResetOnStart = false;
    private int resolutionX = 7680;
    private int resolutionY = 500;
    private bool bNewResolution = false;
    private GameObject activeCam = null;
    #endregion

    #region Setup
    void Start()
    {     
        // Setup OSC
        oscIn = IglooOSC.Instance.oscIn;
        oscIn.Bind("/gameEngine/setCameraType", CreateCameraOSC);
        oscIn.Bind("/gameEngine/getSourceInfo", SendCameraDataOSC);
        oscOut = IglooOSC.Instance.oscOutWarper;

        // Setup Cameras
        CreateDefaultCamera();
        Debug.Log(CreateGraphyTool() ? "Created graphy tool successfully" : "Graphy is not assigned, check documentation Page XX");
        isSetup = true;

    }

    private void CreateCameraOSC(OSCMessage message)
    {
        CreateCamera(message.Values[0].IntValue);
    }

    private void SendCameraDataOSC(OSCMessage message)
    {
        SendCameraData(message.Values[0].IntValue);
    }

    public void setImmersiveRoomResolution(int x, int y) {
        resolutionX = x;
        resolutionY = y;
    }

    #endregion

    #region Camera Creation
    private void CreateDefaultCamera()
    {
        isSendSpoutResetOnStart = ReadCommandLineArgs.ArgumentExists("-iglooSpoutReset");

        if (ReadCommandLineArgs.ArgumentExists("-setTargetFPS"))
        {
            string fpsS = ReadCommandLineArgs.GetValueFollowingArgument("-setTargetFPS");
            int fps = int.Parse(fpsS);
            Debug.Log("IglooCameraCreator -setTargetFPS argument found. Setting target FPS to " + fps);
            Application.targetFrameRate = fps;
        }
        // Vsync overrides target fps setting
        // 0 - Ignore refresh rate, 1 - Update once per screen refresh, 2 - Update every other screen refresh
        else if (ReadCommandLineArgs.ArgumentExists("-setVsync0")) QualitySettings.vSyncCount = 0;
        else if (ReadCommandLineArgs.ArgumentExists("-setVsync1")) QualitySettings.vSyncCount = 1;
        else if (ReadCommandLineArgs.ArgumentExists("-setVsync2")) QualitySettings.vSyncCount = 2;

        // If there is a camera specified with a command line argument this will take priority
        string cameraType = ReadCommandLineArgs.GetValueFollowingArgument("-iglooCameraType");
        if (ReadCommandLineArgs.ArgumentExists("-setNDI"))
        {
            streamType = StreamType.NDI;
        }
        if (ReadCommandLineArgs.ArgumentExists("-setResolution"))
        {
            Debug.Log("Found new resolution for ImmersiveRoom4");
            string newRes = ReadCommandLineArgs.GetValueFollowingArgument("-setResolution");
            string[] cont = newRes.Split('x');
            int.TryParse(cont[0], out resolutionX);
            int.TryParse(cont[1], out resolutionY);
            Debug.Log($"Set new resolution for ImmersiveRoom4 {newRes}");
            bNewResolution = true;
        }

        if (!string.IsNullOrEmpty(cameraType)) 
        {
            if (cameraType == "Cylinder")
            {
                Debug.Log("CylinderCam argument found");
                CreateCamera(0);
                return;
            }

            if (cameraType == ("Cube"))
            {
                Debug.Log("CubeCam argument found");
                CreateCamera(1);
                return;
            }

            if (cameraType == ("Unwrap"))
            {
                Debug.Log("Unwrap argument found");
                CreateCamera(2);
                return;
            }

            if (cameraType == ("Cylinder6"))
            {
                Debug.Log("Cylinder6 argument found");
                CreateCamera(3);
                return;
            }

            if (cameraType == ("ImmersiveRoom4"))
            {
                Debug.Log("ImmersiveRoom4 argument found");
                CreateCamera(4);
                return;
            }
        }


        // If there is no command line argument use the default editor setting
        switch (defaultCamera)
        {
            case CameraType.None:
                Debug.Log("IglooCameraCreator - DefaultCamera is - None");
                break;
            case CameraType.Cylinder:
                Debug.Log("IglooCameraCreator - DefaultCamera is - Cylinder");
                CreateCamera(0);
                break;
            case CameraType.Cube:
                Debug.Log("IglooCameraCreator - DefaultCamera is - Cube");
                CreateCamera(1);
                break;
            case CameraType.Unwrap:
                Debug.Log("IglooCameraCreator - DefaultCamera is - CylinderUnwrap");
                CreateCamera(2);
                break;
            case CameraType.Cylinder6:
                Debug.Log("IglooCameraCreator - DefaultCamera is - Cylinder6");
                CreateCamera(3);
                break;
            case CameraType.ImmersiveRoom4:
                Debug.Log("IglooCameraCreator - DefaultCamera is - ImmersiveRoom4");
                CreateCamera(4);
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Create an Igloo Camera
    /// </summary>
    public void CreateCamera(int index)
    {
        activeCamera = (CameraType)index;
        CreateCamera(iglooCameras[index]);

        if (isSetup)
        {
            // Send spout reset message to Igloo Warper
            oscOut.Send("/spout/reset", OSCValue.Int(1));
        }
        else
        {
            if (isSendSpoutResetOnStart) oscOut.Send("/spout/reset", OSCValue.Int(1));
        }

    }

    // Creates Igloo camera - Touch OSC only supports float
    private void CreateCamera(float index)
    {
        if (index < 0) return;
        CreateCamera((int)index);
    }

    // Destroys existing igloo camera and instatiates a new one
    private void CreateCamera(GameObject camera)
    {

        // Destroy existing Igloo Camera if one exists
        DestroyCamera();

        // Instantiate a new Igloo camera
        activeCam = Instantiate(camera, Vector3.zero, Quaternion.identity) as GameObject;
        if (activeCamera == CameraType.ImmersiveRoom4)
        { 
            if(bNewResolution)
            {
                // Send new resolution to immersiveRoom4
                activeCam.GetComponent<Klak.Ndi.NdiSender>().sourceTexture.height = resolutionY;
                activeCam.GetComponent<Klak.Ndi.NdiSender>().sourceTexture.width = resolutionX;
            }
        }
        else
        {
            activeCam.GetComponent<IglooCamera>().iglooStreamSender = streamType == StreamType.NDI ? IglooCamera.StreamSender.NDI : IglooCamera.StreamSender.Spout;
            activeCam.GetComponent<IglooCamera>().Init();
        }

        // If there is an Igloo player object in the scene, set the camera to follow it
        if (FindObjectOfType<IglooFirstPersonController>())
        {
            GameObject player = FindObjectOfType<IglooFirstPersonController>().gameObject;
            Transform playerCamTrans = player.GetComponentInChildren<Camera>().transform;

            if (activeCam.GetComponent<FollowObjectTransform>()) activeCam.GetComponent<FollowObjectTransform>().objectTransform = playerCamTrans;
        }
        // Otherwise follow specified transform if assigned
        else
        {
            if (additionalSettings.followTransform)
            {
                if (activeCam.GetComponent<FollowObjectTransform>()) activeCam.GetComponent<FollowObjectTransform>().objectTransform = additionalSettings.followTransform;
            }
            else
            {
                if (activeCam.GetComponent<FollowObjectTransform>()) activeCam.GetComponent<FollowObjectTransform>().enabled = false;
            }
        }

        SendCameraData(1);
    }
    #endregion

    /// <summary>
    /// Graphy is a performance monitoring tool distbuted for free by Unity. 
    /// We have implemented this tool as part of the camera package, in order to check for performance issues during testing.
    /// This function creates the tool, and adds it to the scene. 
    /// It can then be enabled using the '#' key.
    /// </summary>
    bool CreateGraphyTool()
    {
        if (!additionalSettings.graphy) return false;
        else
        {
            GameObject graphy = Instantiate(additionalSettings.graphy) as GameObject;
            graphy.transform.SetParent(this.transform, false);
            graphy.transform.localPosition = Vector3.zero;
            graphy.transform.localScale = Vector3.one;
            graphy.transform.localRotation = Quaternion.Euler(Vector3.zero);
            return true;
        }
    }

    /// <summary>
    /// Send information about the active camera via OSC
    /// </summary>
    /// <param name="opened">1 : sender opened, 0: sender closed</param>
    void SendCameraData(int opened)
    {
        if (activeCam.GetComponent<IglooCamera>() == null) return; 

        IglooCamera cam = activeCam.GetComponent<IglooCamera>();
        if (!cam)
        {
            Debug.Log("No Active camera");
            return;
        }

        string sourceType = "";
        string sourceName = cam.cameraName;
        if (cam is IglooUnwrapCamera)
        {
            sourceType = "singleCanvas";

        }
        else if (cam is IglooCamera2D || cam is IglooCamera3D || cam is IglooCubeCameraBasic)
        {
            sourceType = "multiCanvas";
        }


        OSCMessage tempMsg = new OSCMessage("/sourceCreated");
        if (opened == 1)
        {
            tempMsg.Address = "/sourceCreated";
        }
        else if (opened == 0)
        {
            tempMsg.Address = "/sourceClosed";
        }
        tempMsg.AddValue(OSCValue.String(sourceName));
        tempMsg.AddValue(OSCValue.String(sourceType));
        oscOut.Send(tempMsg);
    }

    #region Camera Destruction
    /// <summary>
    /// Destroy Igloo Camera if one exists in the scene
    /// </summary>
    public void DestroyCamera()
    {
 
        if (activeCam)
        {
            SendCameraData(0);
            Debug.Log("IglooCamera object found: " + activeCam.name + "about to destroy");
            DestroyImmediate(activeCam);
        }
        else
            Debug.Log("No IglooCamera exists in scene already");
    }

    void OnDisable()
    {
        SendCameraData(0);
        oscIn.UnbindAll();
    }
    #endregion

}
