﻿using UnityEngine;

public class IglooCamera2D : IglooCamera {

    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();
    }

    private static IglooCamera2D instance;
	
	public static IglooCamera2D Instance {
		get {
			if (instance==null) instance = FindObjectOfType<IglooCamera2D>();
			return instance;
		}
	}

	public override void AddTextureServer (GameObject cameraObject, StreamSender streamSender) {
        switch (streamSender)
        {
            case StreamSender.Spout:
                // Remove NDI Sender if it exists (doubt)  
                if(cameraObject.GetComponent<Klak.Ndi.NdiSender>() != null)
                {
                    Destroy(cameraObject.GetComponent<Klak.Ndi.NdiSender>());
                }
                // Add Spout component
                if (cameraObject.GetComponent<Klak.Spout.SpoutSender>() == null)
                {
                    //Debug.Log("Camera does not have Spout Component, adding one now");
                    //Spout.SpoutSender spout = cameraObject.AddComponent<Spout2SenderCustomResolution>();
                    cameraObject.AddComponent<Klak.Spout.SpoutSender>();
                }
                break;
            case StreamSender.NDI:
                // Remove spout sender if it exists
                if (cameraObject.GetComponent<Klak.Spout.SpoutSender>() != null)
                {
                    Destroy(cameraObject.GetComponent<Klak.Spout.SpoutSender>());
                }
                // Add NDI component
                if (cameraObject.GetComponent<Klak.Ndi.NdiSender>() == null)
                {
                    cameraObject.AddComponent<Klak.Ndi.NdiSender>();
                }
                break;
        }

    }

	public override void RenderTextureSizeSet (int _width, int _height) {
        base.RenderTextureSizeSet(_width, _height);
		foreach (GameObject iglooCam in cameras) {
            RenderTexture rt = new RenderTexture(_width / numberOfCameras, _height, 24, RenderTextureFormat.ARGB32);
            iglooCam.GetComponent<Camera>().targetTexture = rt;
            if (iglooCam.GetComponent<Klak.Spout.SpoutSender>())
            {
                // set the render texture on the spout
                iglooCam.GetComponent<Klak.Spout.SpoutSender>().sourceTexture = rt;
            }
            else
            {
                // set the NDI camera texture
                iglooCam.GetComponent<Klak.Ndi.NdiSender>().sourceTexture = rt;
            }
        }
	}

    public override void FieldOfViewSet(IglooFieldOfView _iglooFieldOfView)
    {
        base.FieldOfViewSet(_iglooFieldOfView);
    }

    public override void OnDestroy()
    {

    }
}
