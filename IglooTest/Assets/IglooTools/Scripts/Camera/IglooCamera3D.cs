﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

[RequireComponent(typeof(CameraControl3D))]
public class IglooCamera3D : IglooCamera2D
{
    public LayerMask cullingMask3D;

    bool startComplete = false;

    public override void Start()
    {
        base.Start();
        startComplete = true;
    }

    public override void Update()
    {
        base.Update();
        if (startComplete) {
            if (cameraControl3D != null)
            {
                //Debug.Log("cameraControl3D object OK");
            }
            else Debug.LogError("cameraControl3D object is null");
        }
    }

    protected CameraControl3D cameraControl3D;

    public CameraControl3D CameraControl3D
    {
        get { return cameraControl3D; }
    }

    protected GameObject cameraParent;
    protected List<GameObject> cameraParentObjects = new List<GameObject>();

    // Camera Settings are initially optimised for 5 cameras
    public override void CreateCameras(GameObject cameraPrefab)
    { 
        for (int i = 0; i < numberOfCameras*2; i++)
        {
            // Create parent for camera pairs
            if (i % 2 == 0) {
                cameraParent = new GameObject();
                int adjI = (int)((i * 0.5f) + 1);
                ApplyParentTransformSettings3D(cameraParent, adjI);
                cameraParentObjects.Add(cameraParent);
            }

            // Uses existing camera prefab and add additional required components
            if (useCameraPrefab && cameraPrefab != null)
            {
                Debug.Log("Using Prefab");
                GameObject cameraObject = Instantiate(cameraPrefab, Vector3.zero, transform.rotation) as GameObject;
                // Add texture server
                AddTextureServer(cameraObject, iglooStreamSender);
                // Add Igloo Fish Eye
                if (cameraPrefab.GetComponent<IglooFisheye>() == null)
                {
                    Debug.Log("Camera Prefab does not have FishEye Component, adding one now");
                    IglooFisheye fisheye = cameraObject.AddComponent<IglooFisheye>();
                    fisheye.fishEyeShader = Shader.Find("IglooFisheyeShader");
                }
                // Remove Audio Component as we don't wan't more than one
                if (cameraObject.GetComponent<AudioListener>())
                {
                    Destroy(cameraObject.GetComponent<AudioListener>());
                    Debug.Log("IglooCamera2D - removing audio listener from camera prefab");
                }
                cameraObject.GetComponent<Camera>().allowHDR = false;
                cameraObject.AddComponent<CameraPerspectiveEditor>();
                cameraObject.transform.parent = cameraParent.transform;
                cameraObject.transform.localRotation = Quaternion.Euler(Vector3.zero);
                cameraObject.transform.localPosition = new Vector3(0, 0, 0);
                cameraObject.name = GetCameraName3D_sequencial(i);
                cameraObject.SetActive(true); 
                cameras.Add(cameraObject);
            }
            // Manually creates camera object and adds required components
            if (!useCameraPrefab)
            { 
                GameObject cameraObject = new GameObject();
                Camera camera = cameraObject.AddComponent<Camera>();
                camera.allowHDR = false;
                cameraObject.AddComponent<CameraPerspectiveEditor>();
                IglooFisheye fisheye = cameraObject.AddComponent<IglooFisheye>();
                fisheye.fishEyeShader = Shader.Find("IglooFisheyeShader");
                AddTextureServer(cameraObject, iglooStreamSender);
                cameraObject.transform.parent = cameraParent.transform;
                cameraObject.transform.localRotation = Quaternion.Euler(Vector3.zero);
                cameraObject.transform.localPosition = new Vector3(0, 0, 0);
                cameraObject.name = GetCameraName3D_sequencial(i);
                //cameraObject.SetActive(false);
                cameraObject.SetActive(true);
                cameras.Add(cameraObject);
            }
            if (useCameraPrefab && cameraPrefab == null) Debug.Log("Igloo Camera Controller: You need to attach a Camera to the Camera Prefab");
        }

        if (GetComponent<CameraControl3D>()) cameraControl3D = GetComponent<CameraControl3D>();
        else Debug.LogError("CameraControl3D not found");

        cameraParentObjects.ForEach((t) =>
        {
            Camera3D camera3D = new Camera3D();
            foreach (Transform child in t.transform)
            {
                //TODO : Fix this for case when num camera is not odd
                if (child.name.Contains(cameraName))
                {
                    string name = child.name;
                    string s_index = name.Substring(cameraName.Length, (name.Length-cameraName.Length));
                    if (!Int32.TryParse(s_index, out int index)) Debug.LogError("IglooCamera2D - String could not be parsed" + name);

                    if (index > 0 && index <= numberOfCameras) {
						camera3D.leftEye = child.GetComponent<Camera>();
					}
					if (index > 0 && index > numberOfCameras) {
						camera3D.rightEye = child.GetComponent<Camera>();
					}					
					/* Adds cameras based on whether index is odd/even
					if (index%2 == 0){
						camera3D.rightEye = child.GetComponent<Camera>();
						Debug.Log ("Added Right Eye" + child.name);
					} 
                        
					else if (index%2 !=0) {
						camera3D.leftEye = child.GetComponent<Camera>();
						Debug.Log ("Added Left Eye" + child.name);
					}
					*/
                        
                }
            }
            if (camera3D != null) {
                cameraControl3D.AddCamera3D(camera3D);
            } 
            else Debug.LogError("cameraControl3D is NULL");
        });
    }


    public virtual void SpoutDimensionsSet(int _width, int _height)
    {
        foreach (GameObject iglooCam in cameras)
        {
            RenderTexture rt = new RenderTexture(_width / numberOfCameras, _height, 24, RenderTextureFormat.ARGB32);
            iglooCam.GetComponent<Camera>().targetTexture = rt;
            if (iglooCam.GetComponent<Klak.Spout.SpoutSender>())
            {
                // set the render texture on the spout
                iglooCam.GetComponent<Klak.Spout.SpoutSender>().sourceTexture = rt;
            }
            else
            {
                // set the NDI camera texture
                iglooCam.GetComponent<Klak.Ndi.NdiSender>().sourceTexture = rt;
            }
        }
    }

    public override void RenderTextureSizeSet(int _width, int _height)
    {
        SpoutDimensionsSet(_width, _height);
    }

    public override void FieldOfViewSet(IglooFieldOfView _iglooFieldOfView)
    {
        base.FieldOfViewSet(_iglooFieldOfView);
    }

    void ApplyParentTransformSettings3D(GameObject parentObject, int i)
    {
        parentObject.transform.parent = gameObject.transform;
        parentObject.transform.localPosition = new Vector3(0, 0, 0);
        parentObject.name = "Camera" + i.ToString();
        parentObject.transform.localRotation = Quaternion.Euler(0, CameraRotY(i-1), 0);
    }

    string GetCameraName3D_leftRight(int i)
    {
        string name = "";
        string eye;
        int index;
        if (i % 2 == 0) {
            eye = "L";
            index = (int)((i * 0.5f) + 1);
        }
        else {
            eye = "R";
            index = (int)((i * 0.5f) + 1);
        } 
        name = cameraName + index + eye;
        return name;
    }

    string GetCameraName3D_sequencial(int i)
    {
        string name = "";
        int index = (int)((i * 0.5f) + 1);
        if (i % 2 != 0) index += numberOfCameras;
        name = cameraName + index;
        return name;
    }

    public virtual void Set3dEnabled(bool state)
    {
        if (cameraControl3D != null) {
            if (!IsSleep) cameraControl3D.Set3dEnabled(state);
            else if (IsSleep) cameraControl3D.is3dEnabled = state;
        }

    }

    public void Set3dSeparation(float value)
    {
        if (cameraControl3D != null)
        {
            cameraControl3D.eyeSpacing = value;
        }
        else {
            Debug.LogError("cameraControl3D object is null");
        } 
    }

    public void Set3dLensShift(float value)
    {
        cameraControl3D.lensShift = value;
    }

    public void Set3dInvert()
    {
        cameraControl3D.Invert();
    }

    public override void SetSleep(bool state)
    {
        base.SetSleep(state);
        if (!state) cameraControl3D.Set3dEnabled(cameraControl3D.is3dEnabled);
    }

    public override void SetCullingMask(LayerMask _cullingMask)
    {
        if(cameraControl3D!=null) cameraControl3D.SetCullingMasks(_cullingMask, cullingMask3D);
    }
}
