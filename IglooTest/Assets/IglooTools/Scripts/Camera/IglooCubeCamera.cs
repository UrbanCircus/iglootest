﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(CameraControl3D))]
[RequireComponent(typeof(CubeCalibrate))]
public class IglooCubeCamera : IglooCamera2D {

    public LayerMask cullingMask3D;
    public int numWalls = 4;

    private GameObject cameraParent;
    public GameObject head;

    CubeCalibrate cubeCalibrate;
    private List<GameObject> cameraParentObjects = new List<GameObject>();

    public override void Start()
    {
        cubeCalibrate = GetComponent<CubeCalibrate>();
        //head = new GameObject();
        //head.name = "Head";
        //head.transform.parent = gameObject.transform;
        //Vector3 pos = gameObject.transform.localPosition;
        //head.transform.position = new Vector3(pos.x = 0.1f, pos.y + 0.1f, pos.z + 0.1f);

        base.Start();
    }

    public override void Update()
    {
        base.Update();
    }

    private CameraControl3D cameraControl3D;

    public CameraControl3D CameraControl3D
    {
        get { return cameraControl3D; }
    }

    public override void CreateCameras(GameObject cameraPrefab)
    {

        for (int i = 0; i < numWalls * 2; i++)
        {
            int adjI = -1;
            adjI = (int)((i * 0.5f) + 1);
            // Create parent for camera pairs
            if (i % 2 == 0)
            {
                cameraParent = new GameObject();
                applyParentTransformSettings3D(cameraParent, adjI);
                cameraParentObjects.Add(cameraParent);
            }

            // Uses existing camera prefab and add additional required components
            if (useCameraPrefab && cameraPrefab != null)
            {
                GameObject cameraObject = Instantiate(cameraPrefab, Vector3.zero, transform.rotation) as GameObject;
                // Add Spout Sender
                if (cameraObject.GetComponent<Klak.Spout.SpoutSender>() == null)
                {
                    Debug.Log("Camera Prefab does not have Spout Component, adding one now");
                    cameraObject.AddComponent< Klak.Spout.SpoutSender>();
                }
                // Remove Audio Component as we don't wan't more than one
                if (cameraObject.GetComponent<AudioListener>())
                {
                    Destroy(cameraObject.GetComponent<AudioListener>());
                    Debug.Log("IglooCamera2D - removing audio listener from camera prefab");
                }
                // Add camera perspective script for off axis projection and assign calibration object and screen index
                if (cameraObject.GetComponent<CameraPerspective>() == null) {
                    CameraPerspective camPerspective = cameraObject.AddComponent<CameraPerspective>();
                    camPerspective.CubeCalibrate = cubeCalibrate;
                    camPerspective.viewPortIndex = adjI-1;
                    camPerspective.estimateViewFrustum = false;
                }

                cameraObject.transform.parent = cameraParent.transform;
                cameraObject.transform.localRotation = Quaternion.Euler(Vector3.zero);
                cameraObject.transform.localPosition = new Vector3(0, 0, 0);
                cameraObject.name = getCameraName3D_sequencial(i);
                cameraObject.SetActive(true);
                cameras.Add(cameraObject);
            }
            // Manually creates camera object and adds required components
            if (!useCameraPrefab)
            {
                GameObject cameraObject = new GameObject();
                cameraObject.AddComponent<Camera>();
                cameraObject.AddComponent<Klak.Spout.SpoutSender>();

                // Perspective script for off axis projection
                CameraPerspective camPerspective = cameraObject.AddComponent<CameraPerspective>();
                camPerspective.CubeCalibrate = cubeCalibrate;
                camPerspective.estimateViewFrustum = false;
                camPerspective.viewPortIndex = adjI-1;

                cameraObject.transform.parent = cameraParent.transform;
                cameraObject.transform.localRotation = Quaternion.Euler(Vector3.zero);
                cameraObject.transform.localPosition = new Vector3(0, 0, 0);
                cameraObject.name = getCameraName3D_sequencial(i);

                cameraObject.SetActive(true);
                cameras.Add(cameraObject);
            }
            if (useCameraPrefab && cameraPrefab == null) Debug.Log("Igloo Camera Controller: You need to attach a Camera to the Camera Prefab");
        }

        cameraControl3D = GetComponent<CameraControl3D>();
        cameraParentObjects.ForEach((t) =>
        {
            Camera3D camera3D = new Camera3D();
            foreach (Transform child in t.transform)
            {
                //TODO : Fix this for case when num camera is not odd
                if (child.name.Contains(cameraName))
                {
                    string name = child.name;
                    string s_index = name.Substring(cameraName.Length, (name.Length - cameraName.Length));
                    int index;
                    if (!Int32.TryParse(s_index, out index)) Debug.LogError("IglooCamera2D - String could not be parsed" + name);

                    if (index > 0 && index <= numberOfCameras)
                    {
                        camera3D.leftEye = child.GetComponent<Camera>();
                    }
                    if (index > 0 && index > numberOfCameras)
                    {
                        camera3D.rightEye = child.GetComponent<Camera>();
                    }
                    /* Adds cameras based on whether index is odd/even
					if (index%2 == 0){
						camera3D.rightEye = child.GetComponent<Camera>();
						Debug.Log ("Added Right Eye" + child.name);
					} 
                        
					else if (index%2 !=0) {
						camera3D.leftEye = child.GetComponent<Camera>();
						Debug.Log ("Added Left Eye" + child.name);
					}
					*/

                }
            }
            cameraControl3D.AddCamera3D(camera3D);
        });
    }


    void spoutDimensionsSet(int _Width, int _Height)
    {
        foreach (GameObject iglooCam in cameras)
        {
            //iglooCam.GetComponent<Spout.SpoutSender>().Width = 1000;
            //iglooCam.GetComponent<Spout.SpoutSender>().Height = 1000;
        }
    }

    public override void RenderTextureSizeSet(int _width, int _height)
    {
        spoutDimensionsSet(_width, _height);
    }

    void applyParentTransformSettings3D(GameObject parentObject, int i)
    {
        parentObject.transform.parent = head.transform;
        parentObject.transform.localPosition = new Vector3(0, 0, 0);
        parentObject.name = "Camera" + i.ToString();
    }

    string getCameraName3D_leftRight(int i)
    {
        string name = "";
        string eye;
        int index;
        if (i % 2 == 0)
        {
            eye = "L";
            index = (int)((i * 0.5f) + 1);
        }
        else
        {
            eye = "R";
            index = (int)((i * 0.5f) + 1);
        }
        name = cameraName + index + eye;
        return name;
    }

    string getCameraName3D_sequencial(int i)
    {
        string name = "";
        int index = (int)((i * 0.5f) + 1);
        if (i % 2 != 0) index += numberOfCameras;
        name = cameraName + index;
        return name;
    }

    public void set3dEnabled(bool state)
    {
        if (!IsSleep) cameraControl3D.Set3dEnabled(state);
        else if (IsSleep) cameraControl3D.is3dEnabled = state;
    }

    public void set3dSeparation(float value)
    {
        cameraControl3D.eyeSpacing = value;
    }

    public void set3dLensShift(float value)
    {
        cameraControl3D.lensShift = value;
    }

    public void set3dInvert()
    {
        cameraControl3D.Invert();
    }

    public override void SetSleep(bool state)
    {
        //base.setSleep(state);
        //if (!state) cameraControl3D.Set3dEnabled(cameraControl3D.is3dEnabled);
    }

    public override void SetCullingMask(LayerMask _cullingMask)
    {
        cameraControl3D.SetCullingMasks(_cullingMask, cullingMask3D);

    }

}
