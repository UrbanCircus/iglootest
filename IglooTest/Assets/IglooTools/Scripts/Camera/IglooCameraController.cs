﻿using UnityEngine;
using System.Collections;
using extOSC;
using System;

public class IglooCameraController : MonoBehaviour {

    private IglooCamera iglooCamera;

    private OSCReceiver oscIn;
    private OSCTransmitter oscOut;

    public float sendMessageInterval = 3.0f;
    private float sendMessageTimer = 0.0f;

    private Camera mainCamera;

    [Range(0.1f,2000f)]
    public float maxClipDistance = 1500;

	private static IglooCameraController instance;
	
	public static IglooCameraController Instance{
		get {
			if (instance == null) instance = FindObjectOfType<IglooCameraController>();
			return instance;
		}
	}

    void OnEnable() {
        IglooSaveController.OnLoad += LoadSettings;
        IglooSaveController.OnSave += SaveSettings;
    }

    void Awake () {
        mainCamera = Camera.main;

#if UNITY_EDITOR_WIN
        Debug.Log("Unity Editor Windows");
		iglooCamera = IglooCamera2D.Instance;
        if (!iglooCamera) Debug.LogError("Can't find instance of Igloo Camera");

#elif UNITY_STANDALONE_WIN
		Debug.Log("Stand Alone Windows");
		iglooCamera = IglooCamera2D.Instance;
        if (!iglooCamera) Debug.LogError("Can't find instance of Igloo Camera");
#endif
    }

    void Start () {
        oscIn = IglooOSC.Instance.oscIn;
        oscOut = IglooOSC.Instance.oscOutWarper;

        //oscIn.Bind("/gameEngine/camera/standardFOV", SetFOVStandardOSC);
        //oscIn.Bind("/gameEngine/camera/wideFOV", SetFOVWideOSC);
        //oscIn.Bind("/gameEngine/camera/superWideFOV", SetFOVSuperWideOSC);
        oscIn.Bind("/gameEngine/3D/separation", Set3dSeparationOSC);
        oscIn.Bind("/gameEngine/3D/lensShift", Set3dLensShiftOSC);
        oscIn.Bind("/gameEngine/3D/enabled", Set3dEnabledOSC);
        oscIn.Bind("/gameEngine/3D/invert", Set3dInvertOSC);
        oscIn.Bind("/gameEngine/cameraTilting", SetTiltOSC);
        oscIn.Bind("/gameEngine/camera/farClipDistance", SetFarClipDistanceOSC);
        oscIn.Bind("/unity/sleep", SetSleepOSC);
        oscIn.Bind("/gameEngine/camera/imageAdjustY", ImageAdjustY);

#if !UNITY_EDITOR
        StartCoroutine(ReadCommandLineArgsAfterTime(3));
#elif UNITY_EDITOR

        // 3D disabled by default in editor
        if (iglooCamera is IglooCamera3D cam3d)
        {
            cam3d.Set3dEnabled(false);
        }
#endif
    }

    private void ImageAdjustY(OSCMessage arg0)
    {
        if(iglooCamera is IglooUnwrapCamera unwrapCamera)
        {
            unwrapCamera.AdjustImageY(arg0.Values[0].FloatValue);
        }
    }


    /*A delay is needed as there  a bug with the spout texture creation,
    where textures won't get assigned to an unactive camera*/
    IEnumerator ReadCommandLineArgsAfterTime(float time)
    {
        Debug.Log("Read Command Line start", this.gameObject);

        yield return new WaitForSeconds(time);
        bool enable3D = ReadCommandLineArgs.ArgumentExists("-igloo3DEnabled");

        if (iglooCamera is IglooCamera3D cam3d)
        {
            cam3d.Set3dEnabled(enable3D);
        }

    }

    void Update() {
        sendMessageTimer += Time.deltaTime;
        if (sendMessageTimer >= sendMessageInterval)
        {
            Send3DInvertState();
            Send3DState();
            SendSleepState();
            sendMessageTimer = 0;
        }
    }

    /// <summary>
    /// Triggered by IglooSaveController OnLoad event
    /// </summary>
    void LoadSettings()
    {
        Debug.Log("IglooCameraController - Gettings Settings");
        IglooData iglooSaveData = IglooSaveController.Instance.iglooSaveData;
        Set3dSeparation(iglooSaveData.cameraSeparation3D);
        Set3dLensShift(iglooSaveData.cameraLensShift3D);
        //SetFOV(iglooSaveData.cameraFOV);
    }

    /// <summary>
    /// Triggered by IglooSaveController OnSave event
    /// </summary>
    void SaveSettings() {
        if (iglooCamera)
        {
            //IglooSaveController.Instance.iglooSaveData.cameraFOV = iglooCamera.iglooFieldOfView;
            if (iglooCamera is IglooCamera3D cam3d)
            {
                IglooSaveController.Instance.iglooSaveData.cameraSeparation3D = cam3d.CameraControl3D.eyeSpacing;
                IglooSaveController.Instance.iglooSaveData.cameraLensShift3D = cam3d.CameraControl3D.lensShift;
            }
        }
        else Debug.LogError("IglooCameraController - SaveSettings - iglooCamera null");
    }

    //public void SetFOV (IglooCamera.IglooFieldOfView fov)
    //{
	//	if (iglooCamera != null)
    //    {
	//		iglooCamera.FieldOfViewSet(fov);
	//		Debug.Log("IglooCameraControl - Igloo Camera FOV set to: " + fov);
	//	}
	//}
	//void SetFOVStandardOSC(OSCMessage message) {
    //    if (message.Values[0].FloatValue == 1)
    //    {
    //        Debug.Log(message);
    //        SetFOV(IglooCamera.IglooFieldOfView.Standard);
    //    }
    //}
	//	
	//void SetFOVWideOSC(OSCMessage message)
    //{
    //    if (message.Values[0].FloatValue == 1)
    //    {
    //        Debug.Log(message);
    //        SetFOV(IglooCamera.IglooFieldOfView.Wide);
    //    }
    //}
    //
    //void SetFOVSuperWideOSC(OSCMessage message)
    //{
    //    if (message.Values[0].FloatValue == 1)
    //    {
    //        Debug.Log(message);
    //        SetFOV(IglooCamera.IglooFieldOfView.SuperWide);
    //    }
    //}

    void SetSleepOSC(OSCMessage message)
    {
        Debug.Log(message);
        if (iglooCamera != null)
        {
            if ((int)message.Values[0].FloatValue == 1)
            {
                iglooCamera.IsSleep = true;
                if (mainCamera != null) mainCamera.enabled = false;
                Time.timeScale = 0.0f;
            }
            else if ((int)message.Values[0].FloatValue == 0)
            {
                iglooCamera.IsSleep = false;
                if (mainCamera != null)mainCamera.enabled = true;
                Time.timeScale = 1.0f;
            }
        }
    }

    void Toggle3dState()
    {
        if (iglooCamera is IglooCamera3D cam3d)
        {
            cam3d.Set3dEnabled(!cam3d.CameraControl3D.is3dEnabled);
        }
    }

    void Set3dEnabledOSC(OSCMessage message)
    {
        Debug.Log(message);
        bool state = message.Values[0].FloatValue == 1 ? true : false;

        if (iglooCamera is IglooCamera3D cam3d)
        {
            cam3d.Set3dEnabled(state);
        }
    }

    public void Set3DEnabled(bool state)
    {
        if (iglooCamera is IglooCamera3D cam3d)
        {
            cam3d.Set3dEnabled(state);
        }
    }

    void Set3dSeparationOSC(OSCMessage message)
    {

        if (iglooCamera is IglooCamera3D cam3d)
        {
            cam3d.Set3dSeparation(message.Values[0].FloatValue);
        }
        else Debug.LogError("igloo camera is not of type iglooSpoutCamera3D");

    }

    /// <summary>
    /// TODO - can't create overload methods as it causes issues with the OSC library,
    /// need to abstact all OSC methods from this class</summary>
    public void Set3dSeparation(float f)
    {
        if (iglooCamera is IglooCamera3D cam3d)
        {
            cam3d.Set3dSeparation(f);
        }
    }

    void Set3dLensShiftOSC(OSCMessage message)
    {
        Debug.Log(message);
        if (iglooCamera is IglooCamera3D cam3d)
        {
            cam3d.Set3dLensShift(message.Values[0].FloatValue);
        }
    }

    public void Set3dLensShift(float f)
    {
        if (iglooCamera is IglooCamera3D cam3d)
        {
            cam3d.Set3dLensShift(f);
        }
    }

    void Set3dInvertOSC(OSCMessage message)
    {
        if (message.Values[0].FloatValue == 1)
        {
            if (iglooCamera is IglooCamera3D cam3d)
            {
                cam3d.Set3dInvert();
            }
        }
    }

    public void Set3dInvert()
    {
        if (iglooCamera is IglooCamera3D cam3d)
        {
            cam3d.Set3dInvert();
        }
    }

    void SetTiltOSC(OSCMessage message)
    {
        Debug.Log(message);
        bool state = message.Values[0].FloatValue == 1 ? true : false;
        SetTilt(state);
    }

    public void SetTilt(bool state)
    {
        iglooCamera.SetTilt(state);
    }


    /// <summary>
    /// Scales input value by maxClipDistance and Sets the far clip distance for all cameras
    /// in the Igloo Camera Array 
    /// </summary>
    /// <param name="message"></param>
    public void SetFarClipDistanceOSC(OSCMessage message)
    {
        message.Values[0].FloatValue *= maxClipDistance;
        iglooCamera.FarClipDistance(message.Values[0].FloatValue);
    }

    /// <summary>
    /// Sets the far clip distance for all cameras in the Igloo Camera Array
    /// </summary>
    /// <param name="value">  </param>
    public void SetFarClipDistance(float value)
    {
        iglooCamera.FarClipDistance(value);
    }

    void Send3DState ()
    {
        if (iglooCamera is IglooCamera3D cam3d)
        {
            int b3D = cam3d.CameraControl3D.is3dEnabled ? 1 : 0;
            string address = "/3D/GE3DEnabled";
            oscOut.Send(address, OSCValue.Int(b3D));
        }
    }

    void Send3DInvertState()
    {
        if (iglooCamera is IglooCamera3D cam3d)
        {
            int inverted = cam3d.CameraControl3D.isInverted ? 1 : 0;
            string address = "/3D/inverted";
            oscOut.Send(address, OSCValue.Int(inverted));
        }
    }

    void SendSleepState()
    {
        int isSleep = iglooCamera.IsSleep ? 1 : 0;
        string address = "/unity/sleep";
        oscOut.Send(address, OSCValue.Int(isSleep));
    }

    void OnDisable()
    {
        IglooSaveController.OnLoad -= LoadSettings;
        IglooSaveController.OnSave -= SaveSettings;

        oscIn.UnbindAll();

    }
}

