﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

public class IglooCamera : MonoBehaviour {
	
	protected List<GameObject> cameras = new List<GameObject> ();


    [Header("Igloo Settings")]
    public int numberOfCameras = 5;
    public string cameraName = "UnityCam";
	public int renderWidth = 8000;
	protected int renderHeight;

	[Space(10)]
    public GameObject player;
	public GameObject firstPersonCamera;
	[Space(10)]
	public GameObject cameraPrefab;
	public bool useCameraPrefab = false;
	[Space(10)]
    [HideInInspector]
	private bool useTilting = false;
    [HideInInspector]
	public bool useSmoothTilting = false;
    [HideInInspector]
	public float smoothTime = 5f;

    [Header("Camera Settings")]
    public CameraClearFlags clearFlags = CameraClearFlags.Skybox;
	public Vector2 clippingPlanes = new Vector2(0.3f,100.0f);
	public enum IglooFieldOfView {Standard, Wide, SuperWide};
	public IglooFieldOfView iglooFieldOfView = IglooFieldOfView.Standard;
    public enum StreamSender { Spout, NDI};
    public StreamSender iglooStreamSender = StreamSender.Spout;
	public float rotationOffset = -144.0f;
	public Color backgroundColour = Color.black;
    public LayerMask cullingMask;

    public bool autoStart = false;

    protected bool isSleep = false;

    protected Vector3 _prevAxis;
    protected float _prevAngle;

    public bool IsSleep {
        get {
            return isSleep;
        }
        set {
            isSleep = value;
            SetSleep(isSleep);
        }
    }

    public virtual void Awake() {
        getCommandLineSettings();
    }

    void getCommandLineSettings() {
        // Get camera name 
        string nameFound = GetSenderName(System.Environment.GetCommandLineArgs());
        if (!string.IsNullOrEmpty(nameFound)) cameraName = nameFound;
        // Get FOV 
        iglooFieldOfView = GetFOV();

    }

	public virtual void Start () {
        DontDestroyOnLoad(this);
        if (firstPersonCamera == null) {
            try
            {
                firstPersonCamera = Camera.main.gameObject;

            }
            catch {
                Debug.Log("IglooCamera - FirstPersonCamera - tilting funtion not available");
            }

        }
			
        if (autoStart) Init();
    }

    public virtual void Init()
    {
        CreateCameras(cameraPrefab);
        SetInitValues();

    }

    protected void SetInitValues () {
        if (useCameraPrefab && cameraPrefab != null) {
            //renderTextureSizeSet(renderWidth, renderHeight);
            FieldOfViewSet(iglooFieldOfView);
        }
        else {
            //renderTextureSizeSet(renderWidth, renderHeight);
            FieldOfViewSet(iglooFieldOfView);
            ClippingPlanesSet(clippingPlanes);
            BackgroundColourSet(backgroundColour);
            //renderTextureSizeSet(renderWidth, renderHeight);
            CameraClearFlagsSet(clearFlags);
            SetCullingMask(cullingMask);
        }

	}
	
	// Camera Settings are initially optimised for 5 cameras
	public virtual void CreateCameras (GameObject cameraPrefab){ 
		for (int i= 0; i < numberOfCameras; i++){
			// Uses existing camera prefab and adds additional required components
			if (useCameraPrefab && cameraPrefab!= null ) {
				GameObject cameraObject= Instantiate (cameraPrefab, Vector3.zero,transform.rotation) as GameObject;
				// Add Spout Sender or NDI Sender
				AddTextureServer(cameraObject, iglooStreamSender);
				// Add Igloo Fish Eye
				if (cameraPrefab.GetComponent<IglooFisheye>()==null) {
					Debug.Log ("Camera Prefab does not have FishEye Component, adding one now");
					IglooFisheye fisheye= cameraObject.AddComponent<IglooFisheye>();
					fisheye.fishEyeShader = Shader.Find("IglooFisheyeShader");
				}
				// Remove Audio Component as we don't wan't more than one
				if (cameraObject.GetComponent<AudioListener>()) {
					Destroy(cameraObject.GetComponent<AudioListener>());
					Debug.Log ("IglooCamera2D - removing audio listener from camera prefab");
				}
				ApplyTransformSettings(cameraObject,i);
				cameras.Add(cameraObject);
			}
			// Manually creates camera object and adds required components
			if (!useCameraPrefab){ 
				GameObject cameraObject= new GameObject();
				cameraObject.AddComponent<Camera>();
				AddTextureServer(cameraObject, iglooStreamSender);
				IglooFisheye fisheye= cameraObject.AddComponent<IglooFisheye>();
				fisheye.fishEyeShader = Shader.Find("IglooFisheyeShader");
				ApplyTransformSettings(cameraObject,i);
				cameraObject.SetActive(false);
				cameraObject.SetActive(true);
				cameras.Add(cameraObject);
			}
			if (useCameraPrefab && cameraPrefab== null) Debug.Log ("Igloo Camera Controller: You need to attach a Camera to the Camera Prefab");
		}
	}
	
	public virtual void AddTextureServer (GameObject cameraObject, StreamSender streamSender) {
		
	}
	
	protected void ApplyTransformSettings (GameObject cameraObject, int i) {
		cameraObject.transform.parent= gameObject.transform;
		cameraObject.transform.localPosition= new Vector3 (0,0,0);
		cameraObject.transform.localScale= new Vector3 (0,0,0);
		cameraObject.name= cameraName + (i+1).ToString();
		cameraObject.transform.localRotation = Quaternion.Euler(0,CameraRotY (i),0);
	}
	
	protected float CameraRotY (int currentCam){
		float rotation = ((360/numberOfCameras)*currentCam)+rotationOffset; // TODO make rotation dynamic for n cameras
		return rotation;
	}
	
	public virtual void FieldOfViewSet (IglooFieldOfView _iglooFieldOfView) {

        iglooFieldOfView = _iglooFieldOfView;
        foreach (GameObject iglooCam in cameras) {
			Rect rect = new Rect(0f,0f,1f,1f);
			float fieldOfView = 0.0f;
			Vector2 fishEyeXY = new Vector2 (0,0);
			
			if (_iglooFieldOfView == IglooFieldOfView.Standard){
                if (numberOfCameras == 5)
                {
                    fieldOfView = 44.46f;
                    fishEyeXY = new Vector2(0.25f, 0);
                    renderHeight = 900;
                }
                else if (numberOfCameras == 6)
                {
                    fieldOfView = 50;
                    fishEyeXY = new Vector2(0.31f, 0);
                    renderHeight = 1290;
                }
            }
            else if (_iglooFieldOfView == IglooFieldOfView.Wide){
                if (numberOfCameras == 5)
                {
                    fieldOfView = 60;
                    fishEyeXY = new Vector2(0.45f, 0);
                    renderHeight = 1270;
                }
                else if (numberOfCameras == 6)
                {
                    fieldOfView = 50;
                    fishEyeXY = new Vector2(0.31f, 0);
                    renderHeight = 1290;
                }

            }
            else if (_iglooFieldOfView == IglooFieldOfView.SuperWide)
            {
                if (numberOfCameras == 5)
                {
                    fieldOfView = 80;
                    fishEyeXY = new Vector2(0.6f, 0);
                    renderHeight = 1850;
                }
                else if (numberOfCameras == 6)
                {
                    fieldOfView = 50;
                    fishEyeXY = new Vector2(0.31f, 0);
                    renderHeight = 1290;
                }
            }
            iglooCam.GetComponent<Camera>().rect= rect;
			iglooCam.GetComponent<Camera>().fieldOfView = fieldOfView;
            if (iglooCam.GetComponent<IglooFisheye>() != null)
			    iglooCam.GetComponent<IglooFisheye>().setXY(fishEyeXY);
            RenderTextureSizeSet(renderWidth, renderHeight);
		}
	}
	
	public void ClippingPlanesSet (Vector2 _clippingPlanes) {
		foreach (GameObject iglooCam in cameras) {
			iglooCam.GetComponent<Camera>().nearClipPlane = _clippingPlanes.x;
			iglooCam.GetComponent<Camera>().farClipPlane = _clippingPlanes.y;
		}
	}
	
	public void BackgroundColourSet (Color _background) {	
		foreach (GameObject iglooCam in cameras) {
			iglooCam.GetComponent<Camera>().backgroundColor = _background;
		}
	}

    public void CameraClearFlagsSet(CameraClearFlags clearFlagType) {
        foreach (GameObject iglooCam in cameras){
            iglooCam.GetComponent<Camera>().clearFlags = clearFlagType;
        }
    }

	
	public virtual void RenderTextureSizeSet (int _width, int _height) {

    }

    public virtual void SetSleep(bool state) {
        foreach (GameObject iglooCam in cameras){
            iglooCam.GetComponent<Camera>().enabled = !state;
        }
    }

    public virtual void SetCullingMask(LayerMask _cullingMask)
    {
        foreach (GameObject iglooCam in cameras)
        {
            iglooCam.GetComponent<Camera>().cullingMask = _cullingMask;
        }
    }

    public virtual void SetTilt(bool _state)
    {
        useTilting = _state;
    }

    public virtual void FarClipDistance(float _value)
    {
        foreach (GameObject iglooCam in cameras)
        {
            iglooCam.GetComponent<Camera>().farClipPlane = _value;
        }
    }

    public virtual void Update () {
        if (useTilting && firstPersonCamera != null)
        {
            TiltCamera();
        }
        else if (!useTilting) {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
	}
	
	void TiltCamera () {

        Vector3 f = firstPersonCamera.transform.forward; // Player's blue axis
        Vector3 f_p = new Vector3(f.x, 0f, f.z).normalized; // Projection of player's blue axis onto the x-z plane of world space
        Vector3 axis; 
        float angle;
        if (f_p.magnitude < Mathf.Epsilon) {
            angle = _prevAngle;
            axis = _prevAxis;
        }
        else {
            angle = 90f - Mathf.Asin(Vector3.Dot(f, f_p)) * Mathf.Rad2Deg;
            axis = -Vector3.Cross(f, f_p).normalized;
            _prevAngle = angle;
            _prevAxis = axis;
        }

        transform.rotation = Quaternion.AngleAxis(angle, axis);
    }
	
	public IEnumerator ReturnToFlat(float tilt, bool clockWise) {

		if (clockWise) {
		float initTilt = tilt;
		Debug.Log (initTilt);
			while(initTilt>0.1f || initTilt<-0.1f){
				
				initTilt = Mathf.Lerp(initTilt,0, 3.0f * Time.deltaTime); 
				float heading = (((player.transform.eulerAngles.y)/360.0f)*(Mathf.PI*2))+(Mathf.PI/2);
				
				Vector3 targetRotation = new Vector3(Mathf.Sin(heading)*initTilt,0,Mathf.Cos(heading)*initTilt);
				
				transform.eulerAngles = targetRotation;
				yield return null;
			}
		}
		
		else if (!clockWise) {
		float initTilt = 360+tilt;
		Debug.Log (initTilt);	
			while(initTilt<359.9f){
				
				initTilt = Mathf.Lerp(initTilt,360, 3.0f * Time.deltaTime); 
				float heading = (((player.transform.eulerAngles.y)/360.0f)*(Mathf.PI*2))+(Mathf.PI/2);
				
				Vector3 targetRotation = new Vector3(Mathf.Sin(heading)*initTilt,0,Mathf.Cos(heading)*initTilt);
				
				transform.eulerAngles = targetRotation;
				yield return null;
			}
		}
    }

    string GetSenderName(string[] args)
    {
        string name = string.Empty;
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i] == "-iglooSenderName")
            {
                name = args[i + 1];
            }
        }
        return name;
    }

    IglooFieldOfView GetFOV()
    {
        IglooFieldOfView newFOV = iglooFieldOfView;
        if (ReadCommandLineArgs.ArgumentExists("-iglooFOV")) {
            string stringFOV = ReadCommandLineArgs.GetValueFollowingArgument("-iglooFOV");
            if (stringFOV == "Stanard" || stringFOV == "stanard") newFOV = IglooFieldOfView.Standard;
            if (stringFOV == "Wide" || stringFOV == "wide") newFOV = IglooFieldOfView.Wide;
            if (stringFOV == "SuperWide" || stringFOV == "superWide" || stringFOV == "superwide") newFOV = IglooFieldOfView.SuperWide;
        }
        return newFOV;
    }

    public virtual void OnDestroy() {

    }

}