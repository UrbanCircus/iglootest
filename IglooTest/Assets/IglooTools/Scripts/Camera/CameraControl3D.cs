﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraControl3D : MonoBehaviour
{
    [Range(0, 1f)]
    public float eyeSpacing = 0.0f;

    [Range(0, 1f)]
    public float lensShift = 0.0f;

    public float eyeSpacingMax = 0.3f;
    public float lensShiftMax = 0.1f;

    [HideInInspector]
    public List<Camera3D> cameras3D = new List<Camera3D>();

    [HideInInspector]
    public bool is3dEnabled = true;
    public bool isInverted = false;

    void Update()
    {
        if (is3dEnabled) {
            SetEyeSpacingHorizontal(eyeSpacing, isInverted);
            SetLensShiftHorizontal(lensShift, isInverted);
        }
    }

    public void AddCamera3D(Camera3D camera)
    {
        cameras3D.Add(camera);
    }

    public void SetEyeSpacingHorizontal(float value, bool invert)
    {
        value *= eyeSpacingMax;
        if (invert) value *= -1;
        value *= 0.5f;

        cameras3D.ForEach((t) =>
        {
            t.leftEye.transform.localPosition = new Vector3(-value, t.leftEye.transform.localPosition.y, t.leftEye.transform.localPosition.z);
            t.rightEye.transform.localPosition = new Vector3(value, t.rightEye.transform.localPosition.y, t.rightEye.transform.localPosition.z);
        });
    }

    public void SetLensShiftHorizontal(float value, bool invert)
    {
        value *= lensShiftMax;
        if (invert) value *= -1;

        cameras3D.ForEach((t) =>
        {
            if (t.leftEye.transform.GetComponent<CameraPerspectiveEditor>() != null)
                t.leftEye.transform.GetComponent<CameraPerspectiveEditor>().lensShift = new Vector2(value, 0);
            if (t.rightEye.transform.GetComponent<CameraPerspectiveEditor>() != null)
                t.rightEye.transform.GetComponent<CameraPerspectiveEditor>().lensShift = new Vector2(-value, 0);
        });
    }

    public void Invert() {
        isInverted = !isInverted;
    }

    public void Set3dEnabled (bool state) {
        is3dEnabled = state;
        if (!is3dEnabled) {
            SetEyeSpacingHorizontal(0,isInverted);
            SetLensShiftHorizontal(0, isInverted);

            cameras3D.ForEach((t) =>
            {
                t.rightEye.transform.GetComponent<Camera>().enabled = false;
            });
        }
        if (is3dEnabled)
        {
            cameras3D.ForEach((t) =>
            {
                t.rightEye.transform.GetComponent<Camera>().enabled = true;
            });
        }
    }

    public void SetCullingMasks(LayerMask _cullingMask, LayerMask _cullingMask3D) {
        cameras3D.ForEach((t) =>
        {
            t.rightEye.transform.GetComponent<Camera>().cullingMask = _cullingMask3D;
            t.leftEye.transform.GetComponent<Camera>().cullingMask = _cullingMask;
        });
    }
}

[System.Serializable]
public class Camera3D {
    public Camera leftEye;
    public Camera rightEye;
}