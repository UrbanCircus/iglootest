﻿using UnityEngine;
using System;

[RequireComponent(typeof(CameraControl3D))]
public class IglooUnwrapCamera : IglooCamera3D
{
    public int numWalls = 4;
    public Igloo.SpoutSenderShaderPass spoutSenderLeft;
    public Igloo.SpoutSenderShaderPass spoutSenderRight;

    GameObject head;

    public override void Start()
    {
        head = new GameObject
        {
            name = "Head"
        };
        head.transform.parent = gameObject.transform;
        Vector3 pos = gameObject.transform.localPosition;
        head.transform.position = new Vector3(pos.x = 0.1f, pos.y + 0.1f, pos.z + 0.1f);

        base.Start();

        spoutSenderLeft.CreateSender(cameraName + "Left");
        spoutSenderRight.CreateSender(cameraName + "Right");
    }


    public override void Update()
    {
        base.Update();
    }

    public override void CreateCameras(GameObject cameraPrefab)
    {
        for (int i = 0; i < numWalls * 2; i++)
        {
            int adjI = -1;
            adjI = (int)((i * 0.5f) + 1);
            // Create parent for camera pairs
            if (i % 2 == 0)
            {
                cameraParent = new GameObject();
                ApplyParentTransformSettings3D(cameraParent, adjI);
                cameraParentObjects.Add(cameraParent);
            }

            // Uses existing camera prefab and add additional required components
            if (useCameraPrefab && cameraPrefab != null)
            {
                GameObject cameraObject = Instantiate(cameraPrefab, Vector3.zero, transform.rotation) as GameObject;
                Camera camera = cameraObject.GetComponent<Camera>();
                camera.allowHDR = false;

                // Remove Audio Component as we don't wan't more than one
                if (cameraObject.GetComponent<AudioListener>())
                {
                    Destroy(cameraObject.GetComponent<AudioListener>());
                    Debug.Log("IglooCamera2D - removing audio listener from camera prefab");
                }

                camera.rect = GetViewportRect(adjI - 1, numWalls);
                HorizontalFieldOfView fov = cameraObject.AddComponent<HorizontalFieldOfView>();
                fov.horitzonalFieldOfView = 90;
                if (i % 2 == 0)
                {
                    camera.targetTexture = spoutSenderLeft.camTexture;
                }
                else
                {
                    camera.targetTexture = spoutSenderRight.camTexture;
                }
                cameraObject.transform.parent = cameraParent.transform;
                cameraObject.transform.localRotation = Quaternion.Euler(Vector3.zero);
                cameraObject.transform.localPosition = new Vector3(0, 0, 0);
                cameraObject.name = GetCameraName3D_sequencial(i);

                cameraObject.SetActive(true);
                cameras.Add(cameraObject);
            }

            // Manually creates camera object and adds required components
            if (!useCameraPrefab)
            {
                GameObject cameraObject = new GameObject();
                Camera camera = cameraObject.AddComponent<Camera>();
                camera.allowHDR = false;

                camera.rect = GetViewportRect(adjI-1, numWalls);
                HorizontalFieldOfView fov = cameraObject.AddComponent<HorizontalFieldOfView>();
                fov.horitzonalFieldOfView = 90;
                if (i%2 == 0)
                {
                    camera.targetTexture = spoutSenderLeft.camTexture;
                }
                else
                {
                    camera.targetTexture = spoutSenderRight.camTexture;
                }
                cameraObject.transform.parent = cameraParent.transform;
                cameraObject.transform.localRotation = Quaternion.Euler(Vector3.zero);
                cameraObject.transform.localPosition = new Vector3(0, 0, 0);
                cameraObject.name = GetCameraName3D_sequencial(i);

                cameraObject.SetActive(true);
                cameras.Add(cameraObject);
            }
            if (useCameraPrefab && cameraPrefab == null) Debug.Log("Igloo Camera Controller: You need to attach a Camera to the Camera Prefab");
        }

        // Add camera pairs to 3D camera controller system 
        cameraControl3D = GetComponent<CameraControl3D>();
        cameraParentObjects.ForEach((t) =>
        {
            Camera3D camera3D = new Camera3D();
            foreach (Transform child in t.transform)
            {
                if (child.name.Contains(cameraName))
                {
                    string name = child.name;
                    string s_index = name.Substring(cameraName.Length, (name.Length - cameraName.Length));
                    if (!Int32.TryParse(s_index, out int index)) Debug.LogError("IglooCamera2D - String could not be parsed" + name);

                    if (index > 0 && index <= numberOfCameras)
                    {
                        camera3D.leftEye = child.GetComponent<Camera>();
                    }
                    if (index > 0 && index > numberOfCameras)
                    {
                        camera3D.rightEye = child.GetComponent<Camera>();
                    }
                }
            }
            cameraControl3D.AddCamera3D(camera3D);
        });
    }

    public override void RenderTextureSizeSet(int _width, int _height)
    {
        // Create new camera textures
        RenderTexture textureLeft = new RenderTexture(_width, _height, 24);
        RenderTexture textureRight = new RenderTexture(_width, _height, 24);
        
        // Assign new textures as camera target textures
        cameraControl3D.cameras3D.ForEach((t) =>
        {
            if (t.leftEye.transform.GetComponent<Camera>() != null) t.leftEye.transform.GetComponent<Camera>().targetTexture = textureLeft;
            if (t.rightEye.transform.GetComponent<Camera>() != null) t.rightEye.transform.GetComponent<Camera>().targetTexture = textureRight;
        });
        
        // Assign new render textures to spout senders
        spoutSenderLeft.camTexture = textureLeft;
        spoutSenderRight.camTexture = textureRight;
    }

    public override void FieldOfViewSet(IglooFieldOfView _iglooFieldOfView)
    {
        iglooFieldOfView = _iglooFieldOfView;
        switch (iglooFieldOfView) {
            case IglooFieldOfView.Standard:
                RenderTextureSizeSet(8000, 1000);
                break;
            case IglooFieldOfView.Wide:
                RenderTextureSizeSet(8000, 1200);
                break;
            case IglooFieldOfView.SuperWide:
                RenderTextureSizeSet(8000, 1400);
                break;
        }
    }

    void ApplyParentTransformSettings3D(GameObject parentObject, int i)
    {
        parentObject.transform.parent = gameObject.transform;
        parentObject.transform.localPosition = new Vector3(0, 0, 0);
        parentObject.name = "Camera" + i.ToString();
        numberOfCameras = 4;
        parentObject.transform.localRotation = Quaternion.Euler(0, CameraRotY(i - 1), 0);
    }

    string GetCameraName3D_leftRight(int i)
    {
        string name = "";
        string eye;
        int index;
        if (i % 2 == 0)
        {
            eye = "L";
            index = (int)((i * 0.5f) + 1);
        }
        else
        {
            eye = "R";
            index = (int)((i * 0.5f) + 1);
        }
        name = cameraName + index + eye;
        return name;
    }

    string GetCameraName3D_sequencial(int i)
    {
        string name = "";
        int index = (int)((i * 0.5f) + 1);
        if (i % 2 != 0) index += 5;
        name = cameraName + index;
        return name;
    }

    public override void SetSleep(bool state)
    {
        base.SetSleep(state);
        if (!state) cameraControl3D.Set3dEnabled(cameraControl3D.is3dEnabled);
    }

    public override void SetCullingMask(LayerMask _cullingMask)
    {
        cameraControl3D.SetCullingMasks(_cullingMask, cullingMask3D);

    }

    public Rect GetViewportRect(int i, int numCams) {
        float width = 1.0f / numCams;
        float height = 1.0f;
        float x = i * width;
        float y = 0.0f;
        Rect viewport = new Rect(x,y,width,height);
        return viewport;
    }

    public override void Set3dEnabled(bool state)
    {
        base.Set3dEnabled(state);
        //if (!state) spoutSenderRight.Close();
        //else if (state) spoutSenderRight.Open();
    }

    public void AdjustImageY(float val)
    {
        spoutSenderLeft.AdjustImageY(val);
        spoutSenderRight.AdjustImageY(val);
    }

}
