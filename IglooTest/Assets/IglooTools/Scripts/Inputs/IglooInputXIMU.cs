﻿using UnityEngine;
using extOSC;

public class IglooInputXIMU : IglooInput {

    private static IglooInputXIMU instance;
    private OSCReceiver oscIn;
    private OSCTransmitter oscOut;
	public static IglooInputXIMU Instance {
		get {
			if (instance == null) instance = GameObject.FindObjectOfType<IglooInputXIMU>();
			return instance;
		}
	}

	void Start () {
        oscIn = IglooOSC.Instance.oscIn;
        oscOut = IglooOSC.Instance.oscOutWarper;
        oscIn.Bind("/ximu/euler", SetRotationValues);
	}

	void Update () {
		if (Input.GetButtonDown("Igloo_XboxRightThumbstick")){
			string address = "/ximu/calibrate";
			Debug.Log ("Sending OSC message using, OSC Address -  " + address);
            oscOut.Send(address, OSCValue.Int(1));		
		}

        /*
#if UNITY_STANDALONE_WIN
        if (Input.GetButton("XboxRightStickPress_WIN"))
        {
            string address = "/ximu/calibrate";
            Debug.Log("Sending OSC message using, OSC Address -  " + address);
            oscOut.Send(address, 1);
        }
#elif UNITY_STANDALONE_OSX
    		if (Input.GetButton("XboxRightStickPress_OSX")){
			string address = "/ximu/calibrate";
			Debug.Log ("Sending OSC message using, OSC Address -  " + address);
            oscOut.Send(address, 1);		
		}
#endif
    */
    }

    void SetRotationValues (OSCMessage message){
		//Debug.Log ("IglooXIMU rotation values -  " + message);	
		heading = -message.Values[2].FloatValue;
		pitch = message.Values[0].FloatValue;
		roll = message.Values[1].FloatValue;
	}

    void OnDisable()
    {
        oscIn.UnbindAll();
    }

}
