﻿using UnityEngine;
using System.Collections;
using extOSC;
public class IglooInputGyrOSC : IglooInput {

	private Vector3 movementAxis;
	private static IglooInputGyrOSC instance;
    private OSCReceiver oscIn;
    private OSCTransmitter oscOut;
    private string gyroIP;  // received from GyrOSC app, needed for sending reorient values
    private int gyroPort;   // received from GyrOSC app, needed for sending reorient values
    private bool isSendGyrOSCSetup = false;

    public bool debug = false;
	public static IglooInputGyrOSC Instance {
		get {
			if (instance == null) instance = GameObject.FindObjectOfType<IglooInputGyrOSC>();
			return instance;
		}
	}

	public Vector3 MovememtAxis 
	{
		get {return movementAxis;}
		set {movementAxis = value;}
	}

    void Start()
    {
        oscIn = IglooOSC.Instance.oscIn;
        oscOut = gameObject.AddComponent<OSCTransmitter>();

        oscIn.Bind("/gyrosc/player/gyro", SetRotationValues);
        oscIn.Bind("/gyrosc/player/button", SetButtonValues);
        oscIn.Bind("/gyrosc/player/ipport", SetIPPort);
    }

    void Update() {
        if (Input.GetButtonDown("Igloo_XboxRightThumbstick"))
            {
            if (!isSendGyrOSCSetup)
            {
                Debug.LogWarning("IglooInputGyrSOC - GyroOSC sending not setup");
            }
            else {
                string address = "/reorient";
                Debug.Log("Sending OSC message using, OSC Address -  " + address);
                OSCValue val = new OSCValue(OSCValueType.Int, 1);
                oscOut.Send(address, val);
            }
        }
    }


    void SetRotationValues (OSCMessage message){
		if (debug) Debug.Log ("IglooGyrOSC rotation values -  " + message);	
		float x = message.Values[0].FloatValue;
		float z = message.Values[1].FloatValue;
		float y = message.Values[2].FloatValue;
		
		pitch = -((x/(Mathf.PI/2))*65.0f);
		heading = -(y/Mathf.PI)*180.0f;
		roll = -((z/(Mathf.PI/2))*65.0f);
	}

	void SetButtonValues (OSCMessage message) {
		if ((int)message.Values[1].IntValue == 1 && (int)message.Values[0].IntValue == 1) movementAxis.x = 1;
		if ((int)message.Values[1].IntValue == 0 && (int)message.Values[0].IntValue == 1) movementAxis.x = 0;
		if ((int)message.Values[1].IntValue == 1 && (int)message.Values[0].IntValue == 2) movementAxis.x = -1;
		if ((int)message.Values[1].IntValue == 0 && (int)message.Values[0].IntValue == 2) movementAxis.x = 0; 
		
		if ((int)message.Values[1].IntValue == 1 && (int)message.Values[0].IntValue == 3) movementAxis.z = -1; 
		if ((int)message.Values[1].IntValue == 0 && (int)message.Values[0].IntValue == 3) movementAxis.z = 0; 
		if ((int)message.Values[1].IntValue == 1 && (int)message.Values[0].IntValue == 9) movementAxis.z = 1; 
		if ((int)message.Values[1].IntValue == 0 && (int)message.Values[0].IntValue ==9) movementAxis.z = 0;

        if ((int)message.Values[1].IntValue == 1 && (int)message.Values[0].IntValue == 8) movementAxis.y = -1;
        if ((int)message.Values[1].IntValue == 0 && (int)message.Values[0].IntValue == 8) movementAxis.y = 0;
        if ((int)message.Values[1].IntValue == 1 && (int)message.Values[0].IntValue == 7) movementAxis.y = 1;
        if ((int)message.Values[1].IntValue == 0 && (int)message.Values[0].IntValue == 7) movementAxis.y = 0;
    }

    void SetIPPort(OSCMessage message) {
        Debug.Log(message);
        string newGyroIP = (string)message.Values[0].StringValue;
        int newGyroPort = (int)message.Values[1].IntValue;

        if (gyroIP != newGyroIP || gyroPort != newGyroPort) {
            gyroIP = newGyroIP;
            gyroPort = newGyroPort;

            CreateGyrOSCSender(gyroIP, gyroPort);
        }
    }

    void CreateGyrOSCSender(string ip, int port) {
        isSendGyrOSCSetup = false;
        if (oscOut.IsAvailable) oscOut.Close();
        oscOut.LocalPort = port;
        oscOut.LocalHost = ip;
        oscOut.Connect();
        if (oscOut.IsAvailable) isSendGyrOSCSetup = true;
    }


    void OnDisable()
    {
        oscIn.UnbindAll();

    }
}
