﻿using UnityEngine;
using System.Collections;

public class IglooInput : MonoBehaviour {

	protected float heading = 0.0f;
	protected float pitch = 0.0f;
	protected float roll = 0.0f;
	
	protected string controllerName = "UnityReceiver";

	public float Heading 
	{
		get {return heading;}
		set {heading = value;}
	}

	public float Pitch 
	{
		get {return pitch;}
		set {pitch = value;}
	}

	public float Roll 
	{
		get {return roll;}
		set {roll = value;}
	}
	
}
