﻿using UnityEngine;
using extOSC;

public class IglooInputController : MonoBehaviour {

	public bool useKeyboardInput;
	public IglooFirstPersonController playerPublic;
	private IglooFirstPersonController player;

    private OSCReceiver oscIn;

	private static IglooInputController instance;
	public static IglooInputController Instance {
		get {
			if (instance == null) instance = FindObjectOfType<IglooInputController>();
			return instance;
		}
	}

    void OnEnable()
    {
        IglooSaveController.OnLoad += LoadSettings;
        IglooSaveController.OnSave += SaveSettings;
    }

    void OnDisable()
    {
        IglooSaveController.OnLoad -= LoadSettings;
        IglooSaveController.OnSave -= SaveSettings;

        oscIn.UnbindAll();
    }

    void Awake () {
		if (playerPublic != null) {
			player = playerPublic;
		}
    }

    void Start() {
        oscIn = IglooOSC.Instance.oscIn;

        // Rotation input setting OSC receivers
        oscIn.Bind("/gameEngine/rotationInput/mouse", SetRotationOSCMouse);
        oscIn.Bind("/gameEngine/rotationInput/gyrOSC", SetRotationOSCGyrOSC);
        oscIn.Bind("/gameEngine/rotationInput/XIMU", SetRotationOSCXIMU);

        oscIn.Bind("/gameEngine/invertPitch", SetInvertPitchOSC);

        // Movement input setting OSC receivers
        oscIn.Bind("/gameEngine/movementInput/unity", SetMovementOSCUnity);
        oscIn.Bind("/gameEngine/movementInput/gyrOSC", SetMovementOSCGyrOSC);

        // Movement Mode setting OSC receivers
        oscIn.Bind("/gameEngine/movementMode/walking", SetMovementOSCWalking);
        oscIn.Bind("/gameEngine/movementMode/flying", SetMovementOSCFlying);
        oscIn.Bind("/gameEngine/movementMode/ghost", SetMovementOSCGhost);

        // Game
        oscIn.Bind("/unity/quit", Quit);
    }

    void Update () {
		if (useKeyboardInput) {

            // First person controller
			if (Input.GetKeyDown(KeyCode.U)) setPlayerRotationInput(IglooFirstPersonController.ROTATION_INPUT.STANDARD);
			if (Input.GetKeyDown(KeyCode.G)) setPlayerRotationInput(IglooFirstPersonController.ROTATION_INPUT.GYROSC);
			if (Input.GetKeyDown(KeyCode.X)) setPlayerRotationInput(IglooFirstPersonController.ROTATION_INPUT.XIMU);
			if (Input.GetKeyDown(KeyCode.G) && Input.GetKey(KeyCode.LeftShift)) setPlayerMovementInput(IglooFirstPersonController.MOVEMENT_INPUT.GYROSC);
			if (Input.GetKeyDown(KeyCode.U) && Input.GetKey(KeyCode.LeftShift)) setPlayerMovementInput(IglooFirstPersonController.MOVEMENT_INPUT.STANDARD);
			if (Input.GetKeyDown(KeyCode.F) && Input.GetKey(KeyCode.LeftShift)) setPlayerMovementMode(IglooFirstPersonController.MOVEMENT_MODE.FLYING);
			if (Input.GetKeyDown(KeyCode.W) && Input.GetKey(KeyCode.LeftShift)) setPlayerMovementMode(IglooFirstPersonController.MOVEMENT_MODE.WALKING);
		}
	}
	
	public void registerPlayer (IglooFirstPersonController _player) {
		
		if (player==null){
			player = _player;
		}
		//else Debug.LogError("IglooController - Player has already been assigned");
	}

    /// <summary>
    /// Triggered by IglooSaveController OnLoad event
    /// </summary>
    private void LoadSettings() {
        IglooData iglooData = IglooSaveController.Instance.iglooSaveData;
        setPlayerMovementInput(iglooData.playerMovementInput);
        setPlayerMovementMode(iglooData.playerMovementMode);
        setPlayerRotationInput(iglooData.playerRotationInput);
        SetInvertPitch(iglooData.invertPitch);
    }

    /// <summary>
    /// Triggered by IglooSaveController OnSave event
    /// </summary>
    private void SaveSettings() {
        IglooSaveController.Instance.iglooSaveData.playerRotationInput  = player.rotationInput;
        IglooSaveController.Instance.iglooSaveData.playerMovementInput  = player.movementInput;
        IglooSaveController.Instance.iglooSaveData.playerMovementMode   = player.movementMode;
        IglooSaveController.Instance.iglooSaveData.invertPitch          = player.m_invertPitch;
    }

    public void setPlayerRotationInput (IglooFirstPersonController.ROTATION_INPUT input) {
		if (player!=null) {
			switch (input)
			{
			case IglooFirstPersonController.ROTATION_INPUT.STANDARD:
				Debug.Log ("IglooController - setting player rotation to MOUSE input");
				player.rotationInput = IglooFirstPersonController.ROTATION_INPUT.STANDARD;
				break;
			case IglooFirstPersonController.ROTATION_INPUT.GYROSC:
				Debug.Log ("IglooController - setting player rotation to GyrOSC input");
				player.rotationInput = IglooFirstPersonController.ROTATION_INPUT.GYROSC;
				break;
			case IglooFirstPersonController.ROTATION_INPUT.XIMU:
				Debug.Log ("IglooController - setting player rotation to XIMU input");
				player.rotationInput = IglooFirstPersonController.ROTATION_INPUT.XIMU;
				break;
			}
		}
		else Debug.Log("Igloo Controller - Player has not been set");
	}
	
	public void setPlayerMovementInput (IglooFirstPersonController.MOVEMENT_INPUT input) {
		if (player!=null) {
			switch (input)
			{
			case IglooFirstPersonController.MOVEMENT_INPUT.STANDARD:
				Debug.Log ("IglooController - setting player movement to Unity input");
				player.movementInput = IglooFirstPersonController.MOVEMENT_INPUT.STANDARD;
				break;
			case IglooFirstPersonController.MOVEMENT_INPUT.GYROSC:
				Debug.Log ("IglooController - setting player movement to GyrOSC input");
				player.movementInput = IglooFirstPersonController.MOVEMENT_INPUT.GYROSC;
				break;
			}
		}
		else Debug.Log("Igloo Controller - Player has not been set");
		
	}  
	
	public void setPlayerMovementMode (IglooFirstPersonController.MOVEMENT_MODE input) {
		if (player!=null) {
			player.movementMode = input;
			Debug.Log("Igloo Controller - setting movement mode to :  " + input);
		}
		else Debug.Log("Igloo Controller - Player has not been set");
		
	} 
	
	void SetRotationOSCMouse (OSCMessage message) {
        Debug.Log(message);
        float value = (float)message.Values[0].FloatValue;
			if (value == 1) {
				setPlayerRotationInput(IglooFirstPersonController.ROTATION_INPUT.STANDARD);
			}
	}

	void SetRotationOSCGyrOSC (OSCMessage message) {
        Debug.Log(message);
        float value = (float)message.Values[0].FloatValue;
		if (value == 1) {
			setPlayerRotationInput(IglooFirstPersonController.ROTATION_INPUT.GYROSC);	
		}
	}
	
	void SetRotationOSCXIMU (OSCMessage message) {
        Debug.Log(message);
        float value = (float)message.Values[0].FloatValue;
		if (value == 1) {
			setPlayerRotationInput(IglooFirstPersonController.ROTATION_INPUT.XIMU);
		}
	}

    void SetInvertPitchOSC(OSCMessage message)
    {
        Debug.Log(message);
        bool value = (bool)message.Values[0].BoolValue;
        SetInvertPitch(value);
    }

    public void SetInvertPitch(bool value)
    {
        Debug.Log("Invert Pitch set to: " + value) ;
        player.m_invertPitch = value;

    }

    void SetMovementOSCUnity (OSCMessage message) {
        Debug.Log(message);
        float value = (float)message.Values[0].FloatValue;
		if (value == 1) {
			setPlayerMovementInput(IglooFirstPersonController.MOVEMENT_INPUT.STANDARD);
		}
	}
	
    void SetMovementOSCGyrOSC (OSCMessage message) {
        Debug.Log(message);
        float value = (float)message.Values[0].FloatValue;
		if (value == 1) {
			setPlayerMovementInput(IglooFirstPersonController.MOVEMENT_INPUT.GYROSC);
		}
	}
	
	void SetMovementOSCWalking (OSCMessage message) {
        Debug.Log(message);
        float value = (float)message.Values[0].FloatValue;
		if (value == 1) {
			setPlayerMovementMode(IglooFirstPersonController.MOVEMENT_MODE.WALKING);
		}
	}
	
	void SetMovementOSCFlying(OSCMessage message) {
        Debug.Log(message);
        float value = (float)message.Values[0].FloatValue;
		if (value == 1) {
			setPlayerMovementMode(IglooFirstPersonController.MOVEMENT_MODE.FLYING);
		}
	}

	void SetMovementOSCGhost(OSCMessage message) {
        Debug.Log(message);
        float value = (float)message.Values[0].FloatValue;
		if (value == 1) {
			setPlayerMovementMode(IglooFirstPersonController.MOVEMENT_MODE.FLYING_GHOST);
		}
	}
			
	public bool PlayerFrozen {
		get {
			return player.PlayerFrozen;
		}
		set {
			player.PlayerFrozen = value;
		}
	}

    void Quit(OSCMessage message)
    {
        Application.Quit();
        Debug.Log(message);
    }
}
