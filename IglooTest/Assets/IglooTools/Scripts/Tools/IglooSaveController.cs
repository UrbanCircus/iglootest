﻿using UnityEngine;
using System.Collections;
using extOSC;

public class IglooSaveController : MonoBehaviour {

    public bool autoLoadInEditor = false;
    private static  IglooSaveController instance;
    public static IglooSaveController Instance {
        get{
            if (instance == null) instance = GameObject.FindObjectOfType<IglooSaveController>();
            return instance;
        }
    }

    private OSCReceiver oscIn;

    [HideInInspector]
    public IglooData iglooSaveData;

    public delegate void LoadAction();
    public static event LoadAction OnLoad;

    public delegate void SaveAction();
    public static event SaveAction OnSave;

    void OnEnable() {
        Debug.Log("IglooSaveController - Start");

        if (!FindObjectOfType<IglooOSC>()) gameObject.AddComponent<IglooOSC>();

        oscIn = IglooOSC.Instance.oscIn;

        oscIn.Bind("/gameEngine/saveSettings", SaveOSC);
        oscIn.Bind("/gameEngine/loadSettings", LoadOSC);

        if (Application.isEditor) {
            if (autoLoadInEditor) StartCoroutine(LoadSettingsAfterTime(3f));
        }
        else StartCoroutine(LoadSettingsAfterTime(3f));
    }

    void OnDisable() {
        oscIn.UnbindAll();
    }

    // Wait needed for 360 cameras to be generated
    public IEnumerator LoadSettingsAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        Load();
    }

    void LoadOSC(OSCMessage msg) {
        if (msg.Values[0].FloatValue == 1)
        {
            Debug.Log("IglooSaveController - Loading");
            Load();
        }
        
    }

    /*Loads the igloo data from a save file and calls OnLoad event*/
    public void Load() {
        if (IglooSaveLoad.LoadIglooData()) {
            iglooSaveData = IglooSaveLoad.iglooSave;
            Debug.Log("IglooSaveController  - Settings loaded OK");
            // Send event for listeners, who should update any parameters
            // contained in igloo save data 
            if (OnLoad != null) OnLoad();
        }
        else {
            Debug.LogError("IglooSaveController - Save file doesn't exist, saving default values");
            Save();
        }
    }

    void SaveOSC(OSCMessage msg) {
        if (msg.Values[0].FloatValue == 1)
        {
            Debug.Log("IglooSaveController - Saving");
            Save();
        } 
    }

    /*Saves igloo data*/
    public void Save() {
        if (iglooSaveData != null)
        {
            if (OnSave != null) OnSave();
            IglooSaveLoad.iglooSave = iglooSaveData;
            IglooSaveLoad.SaveIglooData();
        }
        else Debug.LogError("IglooSaveController - cannot save as iglooSaveData is null");
    }
}
