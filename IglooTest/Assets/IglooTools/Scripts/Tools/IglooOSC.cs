﻿using UnityEngine;
using extOSC;

/// <summary>
/// Class to handle all OSC senders/receivers for Igloo
/// </summary>
public class IglooOSC : MonoBehaviour {

    /// <summary>
    /// OSC receiver which should be used for all Unity Igloo player/camera control
    /// </summary>
    [HideInInspector]
    public OSCReceiver oscIn;
    private int oscInPort = 9007;

    /// <summary>
    /// OSC sender which sends to the Igloo warper application
    /// </summary>
    [HideInInspector]
    public OSCTransmitter oscOutWarper;
    private readonly int oscOutWarperPort = 9001;

    private static IglooOSC instance;
    public static IglooOSC Instance
    {
        get
        {
            if (instance == null) instance = FindObjectOfType<IglooOSC>();
            return instance;
        }
    }

    void Awake () {
        if (!oscIn) oscIn = gameObject.AddComponent<OSCReceiver>();

        // Read OSC port from command line arguments if it exists
        string port = ReadCommandLineArgs.GetValueFollowingArgument("-iglooInOSC");
        if (!string.IsNullOrEmpty(port)) {
            bool success = int.TryParse(port, out int val);
            if (success) oscInPort = val;
        }
        oscIn.LocalPort = oscInPort;
        oscIn.Connect();

        if (!oscOutWarper) oscOutWarper = gameObject.AddComponent<OSCTransmitter>();
        oscOutWarper.LocalPortMode = OSCLocalPortMode.Random;
        oscOutWarper.LocalPort = oscOutWarperPort;
        oscOutWarper.Connect();
        oscOutWarper.RemotePort = oscOutWarperPort;
    }
	
}
