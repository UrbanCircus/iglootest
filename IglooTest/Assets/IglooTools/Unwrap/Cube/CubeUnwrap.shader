﻿Shader "CubeUnwrap"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Scale("Scale", Float) = 1.0
		_Offset("Offset", Float) = 1.0
		[Toggle(USE_LETTERBOX)]
	_isLetterBox("Letterbox", Float) = 0
	}
		SubShader
	{
		Tags{ "RenderType" = "Opaque" }
		LOD 100

		Pass
	{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma shader_feature USE_LETTERBOX

#include "UnityCG.cginc"

		struct appdata
	{
		float4 vertex : POSITION;
		float2 uv : TEXCOORD0;
	};

	struct v2f
	{
		float2 uv : TEXCOORD0;
		float4 vertex : SV_POSITION;
	};

	sampler2D _MainTex;
	float4 _MainTex_ST;
	float _Offset;
	float _Scale;

	v2f vert(appdata v)
	{
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		o.uv = TRANSFORM_TEX(v.uv, _MainTex);
		o.uv.y = 1 - o.uv.y;
		return o;
	}

	fixed4 frag(v2f i) : SV_Target
	{
		float PI = 3.14159265;

	float angle;
	float azim;
	float2 loz;
	float dist;

	float get_s, get_t, pos_s;


	loz = i.uv;
	angle = i.uv.x * 360.0 - 180.0;


	float y = (((i.uv.y - 0.5f) * (1 + (1 - _Scale))) + 0.5f) + _Offset;
	//float y = i.uv.y*(1+(1-_Scale)) - _Offset;

	if (angle >= -180.0 && angle < -90.0) {
		azim = angle + 180.0 - 45.0;
		pos_s = 0.0;
	}

	else if (angle >= -90.0 && angle < 0.0) {
		azim = angle + 45.0;
		pos_s = 0.25;
	}

	else if (angle >= 0.0 && angle < 90.0) {
		azim = angle - 45.0;
		pos_s = 0.5;
	}

	else if (angle >= 90.0 && angle < 180.0) {
		azim = angle - 180.0 + 45.0;
		pos_s = 0.75;
	}

	dist = sqrt(pow(tan(PI*azim / 180.0),2.0) + 1.0);
	get_s = (tan(PI*azim / 180.0) + 1.0) / 8.0 + pos_s;
	get_t = 1 - ((y - 0.5)*dist + 0.5);

	fixed4 col;

#ifdef USE_LETTERBOX
	if (y  < 0.5 - 0.5 / sqrt(2.0) || y > 0.5 + 0.5 / sqrt(2.0)) {
		col = fixed4(0.0, 0.0, 0.0, 0.0);
	}
	else {
		col = tex2D(_MainTex, loz);
	}
#else
	col = tex2D(_MainTex, loz);
#endif				
	return col;
	}
		ENDCG
	}
	}
}
